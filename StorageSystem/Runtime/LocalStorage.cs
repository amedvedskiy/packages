﻿using UnityEngine;
using System.IO;
using System.Text;
using System;
using System.Threading;

namespace SavingSystem
{
    public class LocalStorage<T> where T: class,ISaveUserData, new()
    {
        /// <summary>
        /// Event before save in file, can be user for send data to server
        /// </summary>
        public event Action OnBeforeSave;

        /// <summary>
        /// User Storage
        /// </summary>
        public T UserData
        {
            get
            {
                if (_userData == null)
                    _userData = ReadLocalSaves();
                return _userData;
            }
            private set
            {
                _userData = value;
            }
        }

        private const string SAVE_FILENAME = "data.json";
        private const string PP = "rhhf3zzvlis8pmyc";
        private const bool DONT_ENCRYPT_LOCAL_SAVES = true;
        private string _path => Application.persistentDataPath;
        private T _userData;

        public void SaveLocalSaves()
        {
            UserData.PreSave();
            OnBeforeSave?.Invoke();
            Thread thread = new Thread(WriteLocalSaves);
            thread.Start();
        }

        public void SaveLocalSavesImmediately()
        {
            UserData.PreSave();
            OnBeforeSave?.Invoke();
            WriteLocalSaves();
        }

        private T ReadLocalSaves()
        {
            T data = null;

            if (File.Exists(_path + "/" + SAVE_FILENAME))
            {
                string str = Encoding.UTF8.GetString(File.ReadAllBytes(_path + "/" + SAVE_FILENAME));

#if UNITY_EDITOR
                if (DONT_ENCRYPT_LOCAL_SAVES)
                {
                    try
                    {
                        data = JsonUtility.FromJson<T>(str);
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(new Exception("Cant parce json", e));
                    }

                    if (data != null)
                    {
                        data.PostLoad();
                        return data;
                    }
                }
#endif
                try
                {
                    str = EncryptStringSample.StringCipher.Decrypt(str, PP);
                    data = JsonUtility.FromJson<T>(str);
                    data.PostLoad();
                }
                catch (Exception e)
                {
                    Debug.LogException(new Exception("Cant decrypt saves!", e));
                }
            }
            else
            {
                data = new T();
                data.PostLoad();
            }
            
            return data;
        }
        private void WriteLocalSaves()
        {
#if UNITY_EDITOR
            if (DONT_ENCRYPT_LOCAL_SAVES)
            {
                WriteLocalSaves(Encoding.UTF8.GetBytes(JsonUtility.ToJson(UserData, true)));
                return;
            }
#endif

            var json = EncryptStringSample.StringCipher.Encrypt(JsonUtility.ToJson(UserData), PP);
            WriteLocalSaves(Encoding.UTF8.GetBytes(json));
        }

        private void WriteLocalSaves(byte[] bytes)
        {
            WriteLocalSaves(bytes, SAVE_FILENAME);
        }

        private void WriteLocalSaves(byte[] bytes, string fileName)
        {
            if (bytes.Length < 1) return;

            using (FileStream fileStream = File.Create(_path + "/" + fileName))
            {
                fileStream.Write(bytes, 0, bytes.Length);
                fileStream.Flush();
            }
        }

        //#if UNITY_EDITOR
        //    [UnityEditor.MenuItem("Tools/Save/EncryptSave")]
        //    public static void EncryptSaveCopy()
        //    {
        //        var settings = ResourceManager.GetGeneralSettings();
        //        bool value = settings.DontEncryptLocalSaves;
        //        settings.DontEncryptLocalSaves = true;

        //        LocalStorage storage = new LocalStorage();
        //        var save = storage.ReadLocalSaves();
        //        var data = EncryptStringSample.StringCipher.EncryptToBytes(JsonUtility.ToJson(save,true), Constants.PP);
        //        storage.WriteLocalSaves(data, StorageController.SAVE_FILENAME_COPY);
        //        storage = null;

        //        settings.DontEncryptLocalSaves = value;
        //    }

        //    [UnityEditor.MenuItem("Tools/Save/DecryptSave")]
        //    public static void DecryptSaveCopy()
        //    {
        //        var settings = ResourceManager.GetGeneralSettings();
        //        bool value = settings.DontEncryptLocalSaves;
        //        settings.DontEncryptLocalSaves = false;

        //        LocalStorage storage = new LocalStorage();
        //        var save = storage.ReadLocalSaves();
        //        storage.WriteLocalSaves(Encoding.UTF8.GetBytes(save.ToString()), StorageController.SAVE_FILENAME_COPY);
        //        storage = null;

        //        settings.DontEncryptLocalSaves = value;
        //    }
        //#endif
    }
}
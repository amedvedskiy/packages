# Action System

## Installation
##### [npm.whaleapp](https://npm.whaleapp.team/)

##### Use the Unity Package Manager to install package.

change UnityProject/Packages/manifest.json
##### add:
```
"scopedRegistries": [
    {
      "name": "WhaleApp",
      "url": "https://npm.whaleapp.team",
      "scopes": [
        "com.whaleapp"
      ]
    }
]
```
##### or use Unity ProjectSettings/Package Manager/Scoped Registries
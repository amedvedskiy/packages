﻿using UnityEngine;
using System;
using System.ComponentModel;
using NodeSystem;
namespace ActionSystem
{
    [Serializable]
    public class ActionNode : Node
    {
        public enum State
        {
            Waiting = 0,
            Running = 1,
            RunningDetached = 2,
            Completed = 3
        }

        [DefaultValue(false)]
        public bool DoNotWaitForFinish;
        [NonSerialized]
        public State CurrentState = State.Waiting;
        public IActionOwner ParentQuest { get { return ParentStage.ActionOwner; } }
        public ActionStage ParentStage;

        public bool isRunning()
        {
            if (CurrentState == State.Running || CurrentState == State.RunningDetached) return true;
            return false;
        }

        public virtual void Start()
        {
            CurrentState = State.Running;
            if (DoNotWaitForFinish) CurrentState = State.RunningDetached;
        }

        public virtual void Finish()
        {
            CurrentState = State.Completed;
        }

        public virtual void ForceFinish()
        {
            CurrentState = State.Completed;
        }

        public virtual void Update()
        {

        }

        public bool IsRunning()
        {
            return CurrentState == State.Running;
        }

        public bool IsCompleted()
        {
            return CurrentState == State.Completed;
        }

        //#region Debug

        //[System.NonSerialized]
        //public int invokeId;

        //[ContextMenu("Debug Run")]
        //public void DebugRun()
        //{
        //    var node = this.Clone() as ActionNode;
        //    node.Start();
        //    node.invokeId = InvokeHelper.Invoke(node.DebugDo, 0.0001f, true);
        //}

        //public void DebugDo()
        //{
        //    if (isRunning())
        //        Update();
        //    else
        //        InvokeHelper.StopRepeatedInvoke(invokeId);
        //}
        //#endregion
    }
}

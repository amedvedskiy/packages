﻿using System;

namespace ActionSystem
{
    public class ConditionActionNode : NodeSystem.ConditionNode
    {
        [NonSerialized]
        public ActionStage ParentStage;
    }
}

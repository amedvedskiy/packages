﻿using UnityEngine;
using System.Collections.Generic;
using System.Xml.Serialization;
using System;
namespace ActionSystem
{

    [CreateAssetMenu(menuName = "DialoguesSystem/QuestObject")]
    [XmlRoot("cutscene")]
    public class QuestObject : ScriptableObject, IStagesContainer
    {
        //ovveride name for ignore it in xml
        [XmlIgnore]
        public new string name
        {
            get
            {
                return base.name;
            }
            set
            {
                base.name = value;
            }
        }

        //ovveride name for ignore it in xml
        [XmlIgnore]
        public new HideFlags hideFlags
        {
            get
            {
                return base.hideFlags;
            }
            set
            {
                base.hideFlags = value;
            }
        }

        [XmlAttribute("name")]
        public string cutSceneName = "cutscene1";

        public string Name
        {
            get { return this.name; }
        }

        public string CutsceneName
        {
            get { return this.cutSceneName; }
        }

        [XmlElement("")]
        public List<ActionStageNode> stage = new List<ActionStageNode>();

        public void GetActionStages(ref List<ActionStage> stages)
        {
            for (int i = 0; i < stage.Count; i++)
            {
                var st = stage[i];
                if (!st.enable)
                    continue;

                var convertedStage = new ActionStage();
                st.SetActions(convertedStage);
                stages.Add(convertedStage);

            }
        }

        public ActionNode GetActionStagesStartedFromName(ref List<ActionStage> stages, string stageName, int index)
        {
            bool neededStageWasFound = false;
            ActionNode node = null;
            for (int i = 0; i < stage.Count; i++)
            {
                var st = stage[i];
                if (!st.enable)
                    continue;

                //run always from zero, only in current stage == stagename
                int startIndex = 0;

                if(st.stageName == stageName && !neededStageWasFound)
                {
                    neededStageWasFound = true;
                    startIndex = index;
                }

                if (neededStageWasFound)
                {
                    var convertedStage = new ActionStage();
                    st.SetActions(convertedStage, startIndex);
                    stages.Add(convertedStage);

                    if (st.stageName == stageName)
                        node = convertedStage.ActionQueue[0];
                }
            }
            return node;
        }

        public ActionStage GetStageByName(string stageName)
        {
            var _stage = stage.Find(s => s.stageName == stageName);
            var convertedStage = new ActionStage();
            _stage.SetActions(convertedStage);
            convertedStage.Name = _stage.stageName;
            return convertedStage;
        }


        /// <summary>
        /// Use only for debug
        /// </summary>
        [ContextMenu("Show Cutscene", false, -1000)]
        public void ShowCutscene()
        {
            if (ActionManager.Instance.ReadFromContainer(this, null, false))
                ActionManager.Instance.DoStart();
        }

        /// <summary>
        /// Use only for debug
        /// </summary>
        [ContextMenu("Show Partial Cutscene", false, -1000)]
        public void ShowPartialCutscene()
        {
            ActionNode startNode;
            if (ActionManager.Instance.ReadPartialFromContainer(this, null, "Stage 0", 0, out startNode, false))
            {
                if(startNode is ILoadState)
                {
                    //load node 
                    ((ILoadState)startNode).LoadState(2);
                }
                ActionManager.Instance.DoStart();
            }
        }
        /// <summary>
        /// Use only for debug
        /// </summary>
        public void AddToQueue(bool showIfNotRunning = true)
        {
            if (ActionManager.Instance.IsRunning)
                ActionManager.AddOnFinishCallback(ShowCutscene);
            else if (showIfNotRunning)
                ShowCutscene();
        }

        public void ForEachAction<T>(Action<T> action) where T : ActionNode
        {
            foreach (var s in stage)
            {
                foreach (var a in s.action)
                {
                    if (a is T)
                        action.Invoke(a as T);
                }
            }
        }



#if UNITY_EDITOR
        [ContextMenu("Show Cutscene", true, -1000)]
        private bool ShowCutsceneValidateFunction()
        {
            return Application.isPlaying && ActionManager.HasInstance;
        }

        [ContextMenu("Show Partial Cutscene", true, -1000)]
        private bool ShowPartialCutsceneValidateFunction()
        {
            return Application.isPlaying && ActionManager.HasInstance;
        }
#endif
    }
}


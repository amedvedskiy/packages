﻿using UnityEngine;
using System.Collections.Generic;
namespace ActionSystem
{
    public interface IStagesContainer
    {
        string Name { get; }
        string CutsceneName { get; }

        void GetActionStages(ref List<ActionStage> stages);

        /// <summary>
        /// Get Action from started stage with name stageName and started Indes, will return a first node
        /// </summary>
        /// <returns></returns>
        ActionNode GetActionStagesStartedFromName(ref List<ActionStage> stages,string stageName, int index);
        ActionStage GetStageByName(string stageName);
    }

    public interface IActionsContainer
    {
        void SetActions(ActionStage stage, int index = 0);
    }
}

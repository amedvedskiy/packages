﻿using UnityEngine;
using System.Collections.Generic;
using System;
using NodeSystem;
namespace ActionSystem
{
    public class ActionStageNode : Node, IActionsContainer
    {
        public bool enable = true;
        public string stageName = "stage";
        public List<ActionNode> action = new List<ActionNode>();

        public void SetActions(ActionStage stage, int index = 0)
        {
            for (int i = index; i < action.Count; i++)
            {
                var baseAction = action[i];
                if (baseAction != null)
                {
                    baseAction.CurrentState = ActionNode.State.Waiting;
                    stage.ActionQueue.Add(baseAction);
                    baseAction.ParentStage = stage;
                }
                else
                {
#if UNITY_EDITOR
                    Debug.LogWarningFormat("Node {0} return null actions", action[i].name);
#endif
                }
            }
        }
    }
}


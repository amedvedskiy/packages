﻿using UnityEngine;
using System.Collections;
namespace ActionSystem
{
    /// <summary>
    /// Интерфейс класса исполняющего Actions
    /// </summary>
    public interface IActionOwner
    {
        /// <summary>
        /// Перезаписать имя персонажа?
        /// </summary>
        /// <param name="characterName"></param>
        /// <returns></returns>
        bool TryRenameCharacterName(ref string characterName);

        string Name { get; }
    }
}

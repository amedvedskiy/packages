﻿
using System.Collections.Generic;
using NodeSystem;
namespace ActionSystem
{
    public class ActionStage
    {
        public List<ActionNode> ActionQueue;
        public List<ConditionNode> Conditions;
        public IActionOwner ActionOwner;
        public bool IsRunning;
        public bool IsFinished;
        public string Name;
        public string xmlName;
        public int BaseWeight = 0;
        public int Weight = 0;

        public ActionStage()
        {
            ActionQueue = new List<ActionNode>();
            Conditions = new List<ConditionNode>();
            IsRunning = false;
            IsFinished = false;
        }

        public ActionStage Duplicate(bool clone = false)
        {
            Reset();
            ActionStage a = new ActionStage();
            for (int i = 0; i < ActionQueue.Count; i++)
            {
                var action = (clone ? ActionQueue[i].Clone() : ActionQueue[i]) as ActionNode;
                action.ParentStage = a;
                a.ActionQueue.Add(action);
            }

            a.IsRunning = IsRunning;
            a.IsFinished = IsFinished;
            a.Name = Name;
            a.ActionOwner = ActionOwner;

            return a;
        }

        public ActionStage DuplicateWithConditions(bool clone = false)
        {
            ActionStage a = Duplicate(clone);

            for (int i = 0; i < Conditions.Count; i++)
            {
                var conditions = (clone ? Conditions[i].Clone() : Conditions[i]) as ConditionNode;
                var acond = (ConditionActionNode)conditions;
                if (acond != null)
                    acond.ParentStage = a;

                a.Conditions.Add(conditions);
            }
            a.BaseWeight = BaseWeight;
            a.Weight = Weight;
            return a;
        }

        public void Reset()
        {
            for (int i = 0; i < ActionQueue.Count; i++)
            {
                ActionQueue[i].CurrentState = ActionNode.State.Waiting;
            }
        }

        public bool Start()
        {
            if (ActionQueue.Count > 0)
            {
                IsRunning = true;
                IsFinished = false;

                return true;
            }

            return false;
        }

        public bool Stop()
        {
            if (ActionQueue.Count > 0)
            {
                for (int i = 0; i < ActionQueue.Count; i++)
                {
                    if (ActionQueue[i].CurrentState == ActionNode.State.Running || ActionQueue[i].CurrentState == ActionNode.State.RunningDetached)
                    {
                        ActionQueue[i].Finish();
                    }
                }
            }

            ActionQueue.Clear();

            IsRunning = false;
            IsFinished = true;

            return true;
        }

        public void ForceFinish()
        {
            if (ActionQueue.Count > 0)
            {
                for (int i = 0; i < ActionQueue.Count; i++)
                {
                    if (ActionQueue[i].CurrentState == ActionNode.State.Running)
                    {
                        ActionQueue[i].Finish();
                    }
                    else
                    {
                        ActionQueue[i].ForceFinish();
                    }
                }
            }

            ActionQueue.Clear();
        }

        public bool UpdateConditions()
        {
            if (BaseWeight < 1) return false;
            if (BaseWeight > 10) BaseWeight = 10;

            Weight = BaseWeight;

            for (int i = 0; i < Conditions.Count; i++)
            {
                var a = Conditions[i];
                a.Update();
                if (a.GetState() == false)
                {
                    Weight = 0;
                    return false;
                }
            }

            return true;
        }

        public void Update()
        {
            if (!IsRunning) return;

            bool hasRunning = false;

            if (ActionQueue.Count == 0)
            {
                Stop();
                return;
            }

            for (int i = 0; i < ActionQueue.Count; i++)
            {
                if (ActionQueue[i].isRunning())
                {
                    ActionQueue[i].Update();
                    if (ActionQueue[i].CurrentState == ActionNode.State.Running)
                        hasRunning = true;
                }
            }

            for (int i = 0; i < ActionQueue.Count; i++)
            {
                if (ActionQueue[i].CurrentState == ActionNode.State.Completed)
                {
                    ActionQueue.RemoveAt(i);
                    return;
                }


                if (ActionQueue[i].CurrentState == ActionNode.State.Waiting && (!hasRunning))
                {
                    ActionQueue[i].Start();

                    //Debug.LogFormat("Action {0} in progress (Wait: {1})", ActionQueue[i].GetType().ToString(), ActionQueue[i].DoNotWaitForFinish);

                    //TODO: (Pathfinder) "ActionQueue.Count>0" because somthing wrong with the birds
                    if (ActionQueue.Count > 0 && !ActionQueue[i].DoNotWaitForFinish)
                    {
                        return;
                    }
                }
            }
        }
    }
}

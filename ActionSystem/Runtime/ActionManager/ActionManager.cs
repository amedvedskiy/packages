﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
namespace ActionSystem
{
    public class ActionManager : MonoBehaviour
    {
        #region internal
        internal static ActionManager Instance { get; private set; }
        internal static bool HasInstance => Instance != null;
        #endregion

        //for editor
        public string DebugActionList;
        public ActionNode DebugAction;

        List<ActionStage> StageQueue;
        public bool IsRunning;

        public bool IsPaused
        {
            get { return _isPaused; }
        }

        private bool _isPaused;

        public bool IsSceneChanging
        {
            get;
            private set;
        }

        /// <summary>
        /// Event action on start scene
        /// </summary>
        public static event Action OnQueueStarted;
        /// <summary>
        /// Event action on finish scene
        /// </summary>
        public static event UnityAction OnQueueFinished;
        /// <summary>
        /// One Time Callback on finish scene
        /// </summary>
        private static Action OnFinishCallback;

        public static string ParentAction { get; set; }

        public static void AddOnFinishCallback(Action action)
        {
            OnFinishCallback += action;
        }

        protected void Awake()
        {
            Instance = this;
            StageQueue = new List<ActionStage>();
            IsRunning = false;
        }

        public void AddNodeAtFinish(ActionNode node)
        {
            if (StageQueue.Count == 0)
                return;

            var stage = StageQueue[StageQueue.Count - 1];
            node.ParentStage = stage;
            stage.ActionQueue.Add(node);
        }

        public bool ReadFromContainer(IStagesContainer asset, IActionOwner parent, bool clear = true)
        {
            try
            {
                ParentAction = asset.Name;
                string cutScene = asset.CutsceneName;
                if (clear)
                    StageQueue.Clear();
                asset.GetActionStages(ref StageQueue);
                for (int i = 0; i < StageQueue.Count; i++)
                {
                    StageQueue[i].ActionOwner = parent;
                }
                return true;
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }

            return false;
        }
        public bool ReadPartialFromContainer(IStagesContainer asset, IActionOwner parent,string startedStageName,int index,out ActionNode startedNode, bool clear = true)
        {
            try
            {
                ParentAction = asset.Name;
                string cutScene = asset.CutsceneName;
                if (clear)
                    StageQueue.Clear();
                startedNode = asset.GetActionStagesStartedFromName(ref StageQueue, startedStageName, index);
                for (int i = 0; i < StageQueue.Count; i++)
                {
                    StageQueue[i].ActionOwner = parent;
                }
                return true;
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }
            startedNode = null;
            return false;
        }


        public bool DoStart()
        {
            if (IsRunning)
                return true;

            if (StageQueue.Count > 0)
            {
                StageQueue[0].Start();

                if (IsRunning == false)
                {
                    OnQueueStarted?.Invoke();

                }

                IsRunning = true;
                return true;
            }
            else
            {
                Stop();
            }

            return false;
        }

        public void DoForceComplete()
        {
            FinishActionYetNotFoundType(null);
        }

        public bool Stop()
        {
            StageQueue.Clear();

            IsRunning = false;
            OnQueueFinished?.Invoke();
            OnFinishCallback?.Invoke();
            OnFinishCallback = null;

            return true;
        }

        /// <summary>
        /// stop without callbacks
        /// </summary>
        /// <returns></returns>
        public bool SoftStop()
        {
            StageQueue.Clear();
            IsRunning = false;
            OnFinishCallback = null;

            return true;
        }

        public void NextStage()
        {
            if (StageQueue.Count > 0)
            {
                StageQueue[0].ForceFinish();
                StageQueue.RemoveAt(0);
            }

            if (StageQueue.Count == 0)
            {
                Stop();
            }
            else
            {
                DoStart();
            }
        }

        /// <summary>
        /// Завершить все экшены, пока тип экшена не будет равен actionType
        /// </summary>
        /// <param name="actionType">Тип экшена</param>
        public void FinishActionYetNotFoundType(Type actionType)
        {
            if (StageQueue.Count == 0)
                return;

            if (StageQueue[0].ActionQueue.Count == 0)
                return;

            var quenue = StageQueue[0].ActionQueue;

            for (int i = 0; i < quenue.Count; i++)
            {
                if (!(quenue[i].GetType() == actionType))
                {
                    quenue[i].ForceFinish();
                }
                else
                {
                    return;
                }
            }
        }

        public void Pause(bool value)
        {
            _isPaused = value;
        }

        public void SceneChanging(bool value)
        {
            IsSceneChanging = value;
        }

        public void Update()
        {
            if (!IsRunning) return;
            if (_isPaused) return;

            if (StageQueue.Count == 0)
            {
                Stop();
                return;
            }
#if UNITY_EDITOR
            if (StageQueue[0].ActionQueue.Count > 0)
            {
                DebugAction = StageQueue[0].ActionQueue[0];
                //DebugActionList = StageQueue[0].ActionQueue[0].ParentQuest.ActionsOnFinish;
            }
#endif
            bool needRestart = false;

            for (int i = 0; i < StageQueue.Count; i++)
            {
                if (StageQueue[i].IsRunning)
                {
                    StageQueue[i].Update();
                }

                if (StageQueue[i].IsFinished)
                {
                    StageQueue.RemoveAt(i);
                    IsRunning = false;
                    needRestart = true;
                    break;
                }
            }

            if (needRestart)
            {
                DoStart();
            }
        }
    }
}


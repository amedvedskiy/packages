﻿using UnityEngine;
using UnityEditor;
using System.Xml;
namespace ActionSystem
{
    [CustomEditor(typeof(QuestObject))]
    public class QuestObjectEditor : Editor
    {
        ActionNode actionNode = null;
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            var questObject = target as QuestObject;

            if (GUILayout.Button("Add New Stage"))
            {
                AddStage(questObject);
            }

            if (GUILayout.Button("Remove Last Stage"))
            {
                EditorApplication.Beep();
                if (EditorUtility.DisplayDialog("Remove Stage", "Do you want remove last stage?", "Yes", "Cancel"))
                {
                    RemoveStage(questObject);
                    EditorUtility.SetDirty(this);
                }
            }

            GUILayout.BeginHorizontal();
            actionNode = (ActionNode)EditorGUILayout.ObjectField(actionNode, typeof(ActionNode), true);
            if (GUILayout.Button("Remove Action"))
            {
                if (actionNode)
                {
                    EditorApplication.Beep();
                    if (EditorUtility.DisplayDialog("Remove Idle", "Do you want remove idle?\n(" + actionNode.name + ")", "Yes", "Cancel"))
                    {
                        RemoveNode(questObject, actionNode);
                        EditorUtility.SetDirty(this);
                    }
                    actionNode = null;
                }
            }
            GUILayout.EndHorizontal();
        }


        private XmlDocument ChangeXmlEncoding(XmlDocument xmlDoc, string newEncoding)
        {
            if (xmlDoc.FirstChild.NodeType == XmlNodeType.XmlDeclaration)
            {
                XmlDeclaration xmlDeclaration = (XmlDeclaration)xmlDoc.FirstChild;
                xmlDeclaration.Encoding = newEncoding;
            }
            return xmlDoc;
        }

        void AddStage(QuestObject questObject)
        {
            var node = CreateInstance<ActionStageNode>();
            node.name = "Stage " + questObject.stage.Count;
            node.stageName = "Stage " + questObject.stage.Count;
            questObject.stage.Add(node);

            AssetDatabase.AddObjectToAsset(node, questObject);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        void RemoveStage(QuestObject questObject)
        {
            if (questObject.stage.Count <= 0)
                return;

            var node = questObject.stage[questObject.stage.Count - 1];
            questObject.stage.Remove(node);
            for (int i = 0; i < node.action.Count; i++)
                DestroyImmediate(node.action[i], true);

            DestroyImmediate(node, true);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        private void RemoveNode(QuestObject questObject, ActionNode actionNode)
        {
            foreach (var node in questObject.stage)
            {
                node.action.Remove(actionNode);
                DestroyImmediate(actionNode, true);
            }

            if (actionNode != null)
            {
                if (EditorUtility.DisplayDialog("Remove Idle", "Do you want remove unassegned action?\n(" + actionNode.name + ")", "Yes", "Cancel"))
                {
                    EditorUtility.SetDirty(this);
                    DestroyImmediate(actionNode, true);
                }
            }

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
    }
}

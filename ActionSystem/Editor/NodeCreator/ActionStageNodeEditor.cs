﻿using UnityEditor;
using NodeSystem;

namespace ActionSystem
{
    [CustomEditor(typeof(ActionStageNode))]
    public class ActionStageNodeEditor : Editor
    {
        ReordableNodeList<ActionNode> actionList;

        public ActionStageNode Target
        {
            get { return target as ActionStageNode; }
        }

        private void OnEnable()
        {
            actionList = new ReordableNodeList<ActionNode>(Target.action, this.serializedObject, serializedObject.FindProperty("action"));
        }
        private void OnDisable()
        {
            actionList.OnDisable();
        }

        public override void OnInspectorGUI()
        {
            Target.stageName = EditorGUILayout.TextField("Stage Name", Target.stageName);
            Target.name = Target.stageName;
            Target.enable = EditorGUILayout.Toggle("Enable", Target.enable);
            actionList.OnInspectorGUI();
            serializedObject.ApplyModifiedProperties();
        }
    }
}


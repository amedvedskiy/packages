﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// Order in hierarchy
/// </summary>
public enum Sibling
{
    SetAsFirstSibling,
    SetAsLastSibling
}

public static class InstantiateExtension
{
    /// <summary>
    /// Create new object from template
    /// </summary>
    /// <param name="item">Template item</param>
    public static T InstantiateClone<T>(this T component) where T:Component
    {
        var newItem = Object.Instantiate(component, component.transform.position, component.transform.rotation, component.transform.parent);
        newItem.transform.localScale = component.transform.localScale;
        newItem.transform.SetAsFirstSibling();
        return newItem;
    }


    /// <summary>
    /// Create new object from template
    /// </summary>
    /// <param name="item">Template item</param>
    /// <param name="parent">Parent</param>
   public static T InstantiateClone<T>(this T component, Transform parent, Sibling sibling = Sibling.SetAsLastSibling) where T: Component
    {
        var newItem = Object.Instantiate(component, component.transform.position, component.transform.rotation, parent);
        newItem.transform.localScale = component.transform.localScale;
		switch (sibling)
		{
			case Sibling.SetAsFirstSibling:
				newItem.transform.SetAsFirstSibling();
				break;

			case Sibling.SetAsLastSibling:
				newItem.transform.SetAsLastSibling();
				break;
		}
		return newItem;
    }

    public static T InstantiateClone<T>(this T component, RectTransform parent, Sibling sibling = Sibling.SetAsLastSibling) where T : Component
    {
        var newItem = component.InstantiateClone(parent);

        var itemRectTransform = (RectTransform)component.transform;
        Vector3 anchoredPosition = itemRectTransform.anchoredPosition;
        Vector2 anchorMin = itemRectTransform.anchorMin;
        Vector2 anchorMax = itemRectTransform.anchorMax;
        Vector3 scale = itemRectTransform.localScale;
        Vector2 sizeDelta = itemRectTransform.sizeDelta;


        var newItemRectTransform = (RectTransform)newItem.transform;
        newItemRectTransform.anchoredPosition = anchoredPosition;
        newItemRectTransform.anchorMin = anchorMin;
        newItemRectTransform.anchorMax = anchorMax;
        newItemRectTransform.localScale = scale;
        newItemRectTransform.sizeDelta = sizeDelta;
        return newItem;
    }
}

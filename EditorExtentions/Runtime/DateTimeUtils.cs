﻿using UnityEngine;
using System.Collections;
using System;
using System.Globalization;

public static class DateTimeUtils
{
    public enum TimeOfDay
    {
        Day = 0,
        Night = 1,
        Morning = 2,
        Evening = 3
    }

    public const int SECONDS_IN_DAY = 3600 * 24;
    private static readonly DateTime UnixStartDate = new DateTime(1970, 1, 1, 0, 0, 0, 0);

    private static CultureInfo _enUS = new CultureInfo("en-US");

    public static int GetCurrentTime()
    {
        return (int)(DateTime.UtcNow - UnixStartDate).TotalSeconds;
    }

    public static long GetCurrentTimeInMs()
    {
        return (long)(DateTime.UtcNow - UnixStartDate).TotalMilliseconds;
    }

    public static DateTime UnixTimeToDateTime(int unixTime)
    {
        return UnixStartDate.Add(new TimeSpan(0, 0, unixTime));
    }

    public static int DateTimeToUnixTime(DateTime dateTime)
    {
        return (int)(dateTime - UnixStartDate).TotalSeconds;
    }

    public static bool IsTimeOfDay(TimeOfDay timeOfDay)
    {
        var currentDateTime = DateTime.Now;
        int hour = currentDateTime.Hour;
        switch(timeOfDay)
        {
            case TimeOfDay.Day:
                return hour >= 12 && hour < 18;
            case TimeOfDay.Night:
                return hour < 6;
            case TimeOfDay.Morning:
                return hour >= 6 && hour < 12;
            case TimeOfDay.Evening:
                return hour >= 18;
        }
        return false;
    }
    public static bool IsTimeBetweenDays(int startTime, int day, int daysCount)
    {
        int neededTime = startTime + 3600 * 24 * day;
        int nextDayTime = startTime + 3600 * 24 * (day + daysCount);
        int currentTime = GetCurrentTime();
        return nextDayTime >= currentTime && currentTime >= neededTime;
    }

    public static int ParceDateString(string value)
    {
        const string FORMAT = "yyyy-MM-dd";
        DateTime dt;
        if (DateTime.TryParseExact(value, FORMAT , _enUS, DateTimeStyles.None, out dt))
        {
            return DateTimeUtils.DateTimeToUnixTime(dt);
        }
        return 0;
    }


    /// <summary>
    /// return readable string of time
    /// </summary>
    /// <param name="format">format for ouput string.
    /// For example,"{0:D2}:{1:D2}:{2:D2}"will return 00:00:00
    /// 0 - hours,
    /// 1 - minutes,
    /// 2 - seconds
    /// </param>
    /// <param name="time">time in seconds</param>
    /// <returns></returns>
    public static string GetFormattedString(string format, int time)
    {
        int h = time / 3600;
        int m = (int)((time % 3600f) / 60f);
        int s = (int)(((time % 3600f) % 60f));
        return string.Format(format, h, m, s);
    }
    

    public static DateTime ParseIntDate(int date)
    {
        return date == 0 ? new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc) : new DateTime(date / 10000, (date / 100) % 100, date % 100);
    }

	public static int UnixTimeToLocalTime(int time)
	{
		return time - (int)DateTimeOffset.Now.Offset.TotalSeconds;
	}


}
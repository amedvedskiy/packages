﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

internal sealed class InvokeHelper : MonoBehaviour
{
    private struct InvokeData
    {
        public float endTime;
        public int id;

        private Action m_Action;

        public InvokeData(Action action, float time)
        {
            id = 0;
            m_Action = action;
            endTime = Time.time + time;
        }

        public void Do()
        {
            m_Action?.Invoke();
        }
    }

    private static InvokeHelper _instance;
    private static InvokeHelper Instance
    {
        get
        {
            if (_instance == null)
                CreateInstance();
            return _instance;
        }
    }

    private static void CreateInstance()
    {
        GameObject go = new GameObject("InvokeHelper");
        _instance = go.AddComponent<InvokeHelper>();
        GameObject.DontDestroyOnLoad(go);
    }

    private List<InvokeData> m_Invokes = new List<InvokeData>(64);

    private void Update()
    {
        int count = m_Invokes.Count;
        float time = Time.time;

        for(int i = count - 1; i >= 0;i--)
        {
            InvokeData data = m_Invokes[i];
            if(data.endTime <= time)
            {
                m_Invokes.RemoveAt(i);
                data.Do();
            }
        }

    }

    /// <summary>
    /// Call method after time
    /// </summary>
    /// <param name="action">Method name</param>
    /// <param name="time">Waiting time</param>
    public static int Invoke(Action action, float time)
    {
        var data = new InvokeData(action, time);
        Instance.m_Invokes.Add(data);
        return data.id;
    }

    /// <summary>
    /// Call generic method after time
    /// </summary>
    /// <param name="action">Method name</param>
    /// <param name="arg">generic argument</param>
    /// <param name="time">Waiting time</param>
    public static void Invoke<T>(Action<T> action, T arg, float time)
    {
        Instance.m_Invokes.Add(new InvokeData(() => { action.Invoke(arg); }, time));
    }

    public static void Invoke<T1,T2>(Action<T1,T2> action, T1 arg1, T2 arg2, float time)
    {
        Instance.m_Invokes.Add(new InvokeData(() => { action.Invoke(arg1, arg2); }, time));
    }


    /// <summary>
    /// Call method after YieldInstruction
    /// </summary>
    /// <param name="action">Method name</param>
    /// <param name="time">Yield Instruction</param>
    public static void Invoke(Action action, YieldInstruction time)
    {
        Instance.StartCoroutine(Instance.InvokeCoroutine(action, time));
    }
    /// <summary>
    /// Call method after YieldInstruction
    /// </summary>
    /// <param name="action">Method name</param>
    /// <param name="time">Yield Instruction</param>
    public static void Invoke(Action action, CustomYieldInstruction time)
	{
		Instance.StartCoroutine(Instance.InvokeCoroutine(action, time));
	}

    /// <summary>
    /// Call generic method after YieldInstruction
    /// </summary>
    /// <param name="action">Method name</param>
    /// <param name="time">Yield Instruction</param>
    public static void Invoke<T>(Action<T> action, T arg, YieldInstruction time)
    {
        Instance.StartCoroutine(Instance.InvokeCoroutine(action,arg, time));
    }

    /// <summary>
    /// Call generic method after YieldInstruction
    /// </summary>
    /// <param name="action">Method name</param>
    /// <param name="time">Yield Instruction</param>
    public static void Invoke<T>(Action<T> action, T arg, CustomYieldInstruction time)
	{
		Instance.StartCoroutine(Instance.InvokeCoroutine(action, arg, time));
	}

    /// <summary>
    /// Call method after coroutine
    /// </summary>
    /// <param name="coroutine">Coroutine method</param>
    public static void Invoke(Action action,IEnumerator coroutine)
    {
        Instance.StartCoroutine(Instance.InvokeCoroutine(action, coroutine));
    }

    /// <summary>
    /// Call generic method after coroutine
    /// </summary>
    /// <param name="coroutine">Coroutine method</param>
    public static void Invoke<T>(Action<T> action,T arg, IEnumerator coroutine)
    {
        Instance.StartCoroutine(Instance.InvokeCoroutine(action,arg,coroutine));
    }

    /// <summary>
    /// Call coroutine
    /// </summary>
    /// <param name="coroutine">Coroutine method</param>
    public static void Invoke(IEnumerator coroutine)
    {
        Instance.StartCoroutine(coroutine);
    }

    /// <summary>
    /// Cancel coroutine
    /// </summary>
    /// <param name="coroutine">Coroutine method</param>
    public static void CancelInvoke(IEnumerator coroutine)
    {
        Instance.StopCoroutine(coroutine);
    }

    private IEnumerator InvokeCoroutine(Action action, float time)
    {
        yield return new WaitForSeconds(time);
        action?.Invoke();
    }

    private IEnumerator InvokeCoroutine(Action action, YieldInstruction time)
    {
        yield return time;
        action?.Invoke();
    }
    private IEnumerator InvokeCoroutine(Action action, CustomYieldInstruction time)
	{
		yield return time;
        action?.Invoke();
    }

    private IEnumerator InvokeCoroutine<T>(Action<T> action, T arg, float time)
    {
        yield return new WaitForSeconds(time);
        action?.Invoke(arg);
    }

    private IEnumerator InvokeCoroutine<T>(Action<T> action, T arg, YieldInstruction time)
    {
        yield return time;
        action?.Invoke(arg);
    }

    private IEnumerator InvokeCoroutine<T>(Action<T> action, T arg, CustomYieldInstruction time)
	{
		yield return time;
        action?.Invoke(arg);
    }

    private IEnumerator InvokeCoroutine(Action action, IEnumerator coroutine)
    {
        yield return StartCoroutine(coroutine);
        action?.Invoke();
    }

    private IEnumerator InvokeCoroutine<T>(Action<T> action, T arg, IEnumerator coroutine)
    {
        yield return StartCoroutine(coroutine);
        action?.Invoke(arg);
    }
}

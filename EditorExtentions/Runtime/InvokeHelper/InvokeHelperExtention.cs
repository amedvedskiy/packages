using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class InvokeHelperExtention
{
    /// <summary>
    /// Call method after time
    /// </summary>
    /// <param name="action">Method name</param>
    /// <param name="time">Waiting time</param>
    public static void Invoke(this Action action, float time)
    {
        InvokeHelper.Invoke(action, time);
    }

    /// <summary>
    /// Call generic method after time
    /// </summary>
    /// <param name="action">Method name</param>
    /// <param name="arg">generic argument</param>
    /// <param name="time">Waiting time</param>
    public static void Invoke<T>(this Action<T> action, T arg, float time)
    {
        InvokeHelper.Invoke(action, arg, time);
    }

    public static void Invoke<T1, T2>(this Action<T1, T2> action, T1 arg1, T2 arg2, float time)
    {
        InvokeHelper.Invoke(action, arg1, arg2, time);
    }


    /// <summary>
    /// Call method after YieldInstruction
    /// </summary>
    /// <param name="action">Method name</param>
    /// <param name="time">Yield Instruction</param>
    public static void Invoke(this Action action, YieldInstruction time)
    {
        InvokeHelper.Invoke(action, time);
    }
    /// <summary>
    /// Call method after YieldInstruction
    /// </summary>
    /// <param name="action">Method name</param>
    /// <param name="time">Yield Instruction</param>
    public static void Invoke(this Action action, CustomYieldInstruction time)
    {
        InvokeHelper.Invoke(action, time);
    }

    /// <summary>
    /// Call generic method after YieldInstruction
    /// </summary>
    /// <param name="action">Method name</param>
    /// <param name="time">Yield Instruction</param>
    public static void Invoke<T>(this Action<T> action, T arg, YieldInstruction time)
    {
        InvokeHelper.Invoke(action, arg, time);
    }

    /// <summary>
    /// Call generic method after YieldInstruction
    /// </summary>
    /// <param name="action">Method name</param>
    /// <param name="time">Yield Instruction</param>
    public static void Invoke<T>(this Action<T> action, T arg, CustomYieldInstruction time)
    {
        InvokeHelper.Invoke(action, arg, time);
    }

    /// <summary>
    /// Call method after coroutine
    /// </summary>
    /// <param name="coroutine">Coroutine method</param>
    public static void Invoke(this Action action, IEnumerator coroutine)
    {
        InvokeHelper.Invoke(action, coroutine);
    }

    /// <summary>
    /// Call generic method after coroutine
    /// </summary>
    /// <param name="coroutine">Coroutine method</param>
    public static void Invoke<T>(this Action<T> action, T arg, IEnumerator coroutine)
    {
        InvokeHelper.Invoke(action, arg, coroutine);
    }

    /// <summary>
    /// Call coroutine
    /// </summary>
    /// <param name="coroutine">Coroutine method</param>
    public static void Invoke(this IEnumerator coroutine)
    {
        InvokeHelper.Invoke(coroutine);
    }

    /// <summary>
    /// Call coroutine
    /// </summary>
    /// <param name="coroutine">Coroutine method</param>
    public static void ŅancelInvoke(this IEnumerator coroutine)
    {
        InvokeHelper.Invoke(coroutine);
    }
}

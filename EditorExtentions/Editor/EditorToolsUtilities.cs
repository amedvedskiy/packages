﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace EditorTools
{
	public class Utilities
	{
        private static double _nextTimeUpdate = 0;

        private static bool NeedUpdate
        {
            get
            {
                if (EditorApplication.timeSinceStartup > _nextTimeUpdate)
                {
                    _nextTimeUpdate = EditorApplication.timeSinceStartup + 1f;
                    return true;
                }
                else
                    return false;
            }
        }

		public static bool CancelableProgressBarOperation<T>(string title, T[] array, System.Action<T> func) where T : UnityEngine.Object
		{
			T[] _array = array.OrderBy(e => Path.GetFileNameWithoutExtension(AssetDatabase.GetAssetPath(e))).ToArray();
			for (int i = 0; i < _array.Count(); i++)
			{
				UnityEngine.Object obj = _array[i];
				string path = AssetDatabase.GetAssetPath(obj);

				float progress = (float)i / _array.Count();
				if (EditorUtility.DisplayCancelableProgressBar(title, string.Format("{0}/{1}\t{2}", i, _array.Count(), path), progress))
				{
					EditorUtility.ClearProgressBar();
					return false;
				}

				try
				{
					func.Invoke(_array[i]);
				}
				catch (System.Exception e)
				{
					Debug.LogWarningFormat(obj, "{0}\t{1}", path, e);
				}
			}
			EditorUtility.ClearProgressBar();
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
			return true;
		}

		public static bool SimpleCancelableProgressBarOperation<T>(string title, IEnumerable<T> array, System.Action<T> func)
		{
			IEnumerable<T> _array = array;
			for (int i = 0; i < _array.Count(); i++)
			{
				float progress = (float)i / _array.Count();

				if(NeedUpdate)
					if (EditorUtility.DisplayCancelableProgressBar(title,string.Format("{0}/{1}", i, _array.Count()), progress))
					{
						EditorUtility.ClearProgressBar();
						return false;
					}

				try
				{
					func.Invoke(_array.ElementAt(i));
				}
				catch (System.Exception e)
				{
					Debug.Log(e, _array.ElementAt(i) as UnityEngine.Object);
				}
			}
			EditorUtility.ClearProgressBar();
			return true;
		}

		public static void DisplayDialog(Action okAction, Action cancelAction, string title, string message, string ok = "OK", string cancel = "Cancel")
		{
			if (EditorUtility.DisplayDialog(title, message, ok, cancel))
			{
				okAction?.Invoke();
			}
			else
			{
				cancelAction?.Invoke();
			}
		}

		public static void ProgressBar(float value, string text)
		{
			Rect r = EditorGUILayout.BeginVertical();
			EditorGUI.ProgressBar(r, value, text);
			GUILayout.Space(16);
			EditorGUILayout.EndVertical();
		}

        public static int SelectionPopup(Rect position, SerializedProperty prop, GUIContent label, string[] array)
        {
            int value = Array.FindIndex(array, x => x == prop.stringValue);
            if (value == -1)
                value = 0;
            value = EditorGUI.Popup(position, label.text, value, array);
            string stringValue = array[value];
            prop.stringValue = stringValue;

            return value;
        }

        [MenuItem("CONTEXT/Transform/Select parent")]
		public static void SelectRootObject(MenuCommand command)
		{
			Transform transform = (Transform)command.context;
			var parent = transform.parent;
			if (parent)
				Selection.objects = new UnityEngine.Object[] { parent.gameObject };
		}

		[MenuItem("CONTEXT/ScriptableObject/Set Dirty")]
		public static void SetDirty(MenuCommand command)
		{
			ScriptableObject so = (ScriptableObject)command.context;
			EditorUtility.SetDirty(so);
		}
		[MenuItem("CONTEXT/ScriptableObject/Set Main")]
		public static void SetAsMain(MenuCommand command)
		{
			string path = AssetDatabase.GetAssetPath(command.context);
			AssetDatabase.SetMainObject(command.context, path);
			EditorUtility.SetDirty(command.context);
			AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
		}

		[MenuItem("CONTEXT/ScriptableObject/Not Editable Trigger")]
		public static void ScriptableObjectNotEditable(MenuCommand command)
		{
			ScriptableObject so = (ScriptableObject)command.context;
			so.hideFlags = so.hideFlags == HideFlags.NotEditable ? HideFlags.None : HideFlags.NotEditable;
			EditorUtility.SetDirty(so);
		}

		[MenuItem("CONTEXT/Button/OnClick Invoke")]
		public static void ButtonOnClickInvoke(MenuCommand command)
		{
			UnityEngine.UI.Button target = (UnityEngine.UI.Button)command.context;
			target.onClick.Invoke();
		}
		[MenuItem("CONTEXT/Canvas/CanvasSortingLayer - UI")]
		public static void CanvasSortingLayer(MenuCommand command)
		{
			Canvas target = (Canvas)command.context;
			target.sortingLayerName = "UI";
		}

		[MenuItem("CONTEXT/SkinnedMeshRenderer/RecalculateBounds")]
		public static void SetDirtys(MenuCommand command)
		{
			SkinnedMeshRenderer mr = (SkinnedMeshRenderer)command.context;
			mr.sharedMesh.RecalculateBounds();
			mr.localBounds = mr.sharedMesh.bounds;
		}

        [MenuItem("CONTEXT/Sprite/SelectAtlas")]
        public static void SelectAtlas(MenuCommand command)
        {
            Sprite sprite = (Sprite)command.context;

            string atlasName;
            Texture2D texture;
            UnityEditor.Sprites.Packer.GetAtlasDataForSprite(sprite, out atlasName, out texture);
            if (texture)
                Selection.objects = new UnityEngine.Object[] { texture };
        }
    }
}

﻿using System.Collections.Generic;
using UnityEngine;

namespace Services
{
    public sealed class ProjectContext : MonoBehaviour
    {
        private static readonly ServiceContainer container = new ServiceContainer();
        internal static ServiceContainer Container => container;

        [SerializeField] private List<MonoInstaller> _installers = new List<MonoInstaller>();

        private void Start()
        {
            InstallBindings();
        }

        public void InstallBindings()
        {
            foreach (var i in _installers)
                i.InstallBindings(container);

            container.ForEach<IInitialize>((x) => x.Initialize());
        }

        //only for internal use
        internal static T GetService<T>()
        {
            return container.GetService<T>();
        }
    }
}

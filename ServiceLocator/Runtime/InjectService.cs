using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Services
{
    public struct InjectService<T>
    {
        private T _service;
        public T Service 
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get 
            {
                if (_service == null)
                    _service = ProjectContext.GetService<T>();
                return _service; 
            } 
        }
    }
}

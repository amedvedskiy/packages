using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Services
{
    public class ServiceContainer
    {
        private List<object> _services = new List<object>(128);


        public void Bind<T>() where T: new()
        {
            var service = new T();
            _services.Add(service);
        }

        public void Bind(object service)
        {
            _services.Add(service);
        }

        public void UnBind(object service)
        {
            _services.Remove(service);
        }

        internal T GetService<T>()
        {
            for(int i = 0; i < _services.Count; i++)
            {
                var s = _services[i];
                if (s is T)
                    return (T)s;
            }
            return default;
        }

        internal void ForEach<T>(Action<T> action)
        {
            for (int i = 0; i < _services.Count; i++)
            {
                var s = _services[i];
                if (s is T)
                    action.Invoke((T)s);
            }
        }

    }
}

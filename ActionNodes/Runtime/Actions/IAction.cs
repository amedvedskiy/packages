using System.Threading;
using System.Threading.Tasks;

namespace Actions
{
    public interface IAction
    {
        bool DontWait { get; }

        /// <summary>
        /// Run Action async
        /// </summary>
        /// <returns></returns>
        Task RunAsync(CancellationToken cancellationToken);

        /// <summary>
        /// Called when action should be stoped
        /// </summary>
        void Stop();

        internal void SetParent(IActionRunner runner);
    }
}

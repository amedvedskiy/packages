using System;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;


namespace Actions
{
    [ManagedReference.ManagedReferenceGroup("Wait")]
    [Serializable]
    public class WaitAction : IAction
    {
        [SerializeField]
        private bool _dontWait;
        [SerializeField]

        private int _delayInSeconds;
        public bool DontWait => _dontWait;

        public void Stop()
        {

        }

        public async Task RunAsync(CancellationToken cancellationToken)
        {
            await Task.Delay(_delayInSeconds * 1000, cancellationToken);
            Debug.Log("WaitAction finish async");
        }
    }
}

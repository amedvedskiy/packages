using System;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;


namespace Actions
{
    [ManagedReference.ManagedReferenceGroup("Debug")]
    [Serializable]
    public class DebugLogAction : IAction
    {
        [SerializeField]
        private bool _dontWait;
        [SerializeField]

        private string _value;
        public bool DontWait => _dontWait;

        public void Stop()
        {

        }

        public Task RunAsync(CancellationToken cancellationToken)
        {
            Debug.Log(_value);
            return Task.CompletedTask;
        }
    }
}

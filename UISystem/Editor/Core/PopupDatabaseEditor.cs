﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using NodeSystem;
namespace UISystem
{
	[CustomEditor(typeof(PopupDatabase))]
	public class PopupDatabaseEditor : Editor
	{
		PopupDatabase Target
		{
			get
			{
				return target as PopupDatabase;
			}
		}

		const string PROPERTY_NAME = "popups";
		SimpleReorderableList list;

		private void OnEnable()
		{
			list = new SimpleReorderableList(serializedObject, serializedObject.FindProperty(PROPERTY_NAME));
		}


		public override void OnInspectorGUI()
		{
			SerializedProperty prop = serializedObject.GetIterator();
			prop.NextVisible(true);
			while (prop.NextVisible(false))
			{
				if (prop.name == PROPERTY_NAME)
				{
					list.DoLayoutList();
				}
				else
				{
					EditorGUILayout.PropertyField(prop);
				}
			}
		}

	}
}
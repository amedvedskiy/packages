﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
namespace NodeSystem
{
	/// <summary>
	/// Editor for list
	/// </summary>
	public class SimpleReorderableList
	{
		public ReorderableList List { get; protected set; }

		public SerializedObject TargetObject { get; set; }
		public SerializedProperty TargetProperty { get; set; }
		public string listName = "";
		const float UNVISIBLE_PROPERTY_SIZE = 20f;

		protected Dictionary<int, SerializedProperty> props = new Dictionary<int, SerializedProperty>();

		public SimpleReorderableList(SerializedObject targetObject, SerializedProperty targetProperty)
		{
			TargetObject = targetObject;
			TargetProperty = targetProperty;

			//создает ReorderableList
			List = new ReorderableList(TargetObject,
											targetProperty,
											true, true, true, true);

			List.drawElementCallback = OnDrawElementCallback;

			//List.onSelectCallback = OnSelectCallback;
			List.elementHeightCallback = OnElementHeightCallback;
			//List.drawHeaderCallback = OnDrawHeaderCallback;
			List.onReorderCallback = OnReorderCallback;
			//List.onCanRemoveCallback = OnCanRemoveCallback;
			//List.onRemoveCallback = OnRemoveCallback;
			//List.onAddDropdownCallback = OnAddDropdownCallback;
		}
		public virtual void DoLayoutList()
		{
			TargetObject.UpdateIfRequiredOrScript();
			List.DoLayoutList();
			TargetObject.ApplyModifiedProperties();
		}
		public virtual void DoList(Rect rect)
		{
			TargetObject.UpdateIfRequiredOrScript();
			List.DoList(rect);
			TargetObject.ApplyModifiedProperties();
		}
		const float marginY = 2f;
		const float marginX = 10f;
		protected virtual void OnDrawElementCallback(Rect rect, int index, bool isActive, bool isFocused)
		{
			SerializedProperty prop = GetPropertyAtIndex(index);
			EditorGUI.PropertyField(new Rect(rect.x + marginX, rect.y + marginY, EditorGUIUtility.currentViewWidth - 70, EditorGUIUtility.singleLineHeight), prop, true);

			if (prop.isExpanded)
				rect.y += EditorGUI.GetPropertyHeight(prop) + marginY;
			else
				rect.y += EditorGUIUtility.singleLineHeight;
		}

		protected virtual float OnElementHeightCallback(int index)
		{
			SerializedProperty element = GetPropertyAtIndex(index);
			return CalculatepropertyHeight(element);
		}

		protected float CalculatepropertyHeight(SerializedProperty prop)
		{
			float height = 4f;

			if (prop.isExpanded)
				height += EditorGUI.GetPropertyHeight(prop);
			else
				height += EditorGUIUtility.singleLineHeight;

			return height;
		}

		public SerializedProperty GetPropertyAtIndex(int index)
		{
			if (props.ContainsKey(index))
				return props[index];

			var prop = TargetProperty.GetArrayElementAtIndex(index);
			props.Add(index, prop);
			return prop;
		}

		protected virtual void OnReorderCallback(ReorderableList list)
		{
			props.Clear();
		}
	}
}
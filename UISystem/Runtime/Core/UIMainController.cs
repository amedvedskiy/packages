﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace UISystem
{
    public class UIMainController : MonoBehaviour
    {
        protected class Constants
        {
            public const float NOT_INTERACTABLE_ALPHA = 0.5f;
            public const float QUEUE_OPEN_DELAY = 0.5f;
            public const int MOVE_ALERT_LIMIT = 5;
        }

        public AnchorsController anchorController = new AnchorsController();

        [Expanded,SerializeField] private ReferenceWindow[] preloadedWindows;
        [SerializeField] private RectTransform windowsRoot;
        [SerializeField] private RectTransform sortedWindowsRoot;
        [SerializeField] private RectTransform preloadedWindowsRoot;
        [SerializeField] private UIWindowAnimationController _animationController;

        [SerializeField]
        protected CanvasScaler canvasScaler;
        public CanvasScaler CanvasScaler
        {
            get { return canvasScaler; }
        }

        [SerializeField]
        protected Canvas canvas;
        public Canvas Canvas
        {
            get { return canvas; }
        }

        /// <summary>
        /// u can set up a default popupdatabase, or use a set method for late initialization(like bundles)
        /// </summary>
        [SerializeField]
        private BasePopupDatabase _popupDatabase;
        private Dictionary<string, UIBaseWindow> _dict = new Dictionary<string, UIBaseWindow>();
        private List<int> indexes = new List<int>();
        private List<UIBaseWindow> _activeWindowsQueue = new List<UIBaseWindow>();
        private Queue<UIBaseWindow> _toOpenWindowsQueue = new Queue<UIBaseWindow>();

        public event Action<string> OnWindowAdded;
        public event Action<string> OnWindowRemoved;
        public event Action<UIBaseWindow> OnWindowOpened;
        public event Action<UIBaseWindow> OnWindowClosed;

        #region Interface

        public void SetExternalDatabase(BasePopupDatabase popupDatabase)
        {
            _popupDatabase = popupDatabase;
        }

        public bool HasWindow(string nameId)
        {
            return _dict.ContainsKey(nameId);
        }
#if UISYSTEM_ADDRESSABLES
        public async Cysharp.Threading.Tasks.UniTask<T> GetWindowAsync<T>(string name) where T : UIBaseWindow
        {
            if (HasWindow(name))
                return _dict[name] as T;

            var db = _popupDatabase as AddresablePopupDatabase;
            int index = db.GetIndex(name);
            var window = await db.GetWindowAsync<T>(name);
            SetParentAndSibling(window, index);
            window.name = name;
            AddWindow(window);
            return window;
        }

        private void SetParentAndSibling(UIBaseWindow window, int index)
        {
            var root = index > -1 ? this.sortedWindowsRoot : this.windowsRoot;
            window.transform.SetParent(root, false);
            if (index > -1)
            {
                int i = GetLocalIndex(index);
                window.transform.SetSiblingIndex(i);
            }
        }
#else
        public T GetWindow<T>(string nameId) where T : UIBaseWindow
        {
            if (_dict.ContainsKey(nameId))
            {
                return _dict[nameId] as T;
            }
            else
            {
                int index;
                var prefab = _popupDatabase.GetWindow<T>(nameId, out index);

                if (prefab == null)
                    return null;

                return LoadWindowByPrefab(prefab, true, index);
            }
        }
        private T LoadWindowByPrefab<T>(T prefabComponent, bool attachToUIRoot = false, int index = -1) where T : UIBaseWindow
        {
            T window = attachToUIRoot
                ? (Instantiate(prefabComponent, index > -1 ? sortedWindowsRoot : windowsRoot, false))
                : Instantiate(prefabComponent);

            AddWindow(window);

            if (index > -1)
            {
                int i = GetLocalIndex(index);
                window.transform.SetSiblingIndex(i);
            }

            window.name = prefabComponent.name;
            return window;
        }
#endif

        public void AddWindow(UIBaseWindow window)
        {
            _dict.Add(window.nameId, window);
            window.CreateWindow(this);
            OnWindowAdded?.Invoke(window.nameId);
        }

        public void RemoveWindow(UIBaseWindow window)
        {
            OnWindowRemoved?.Invoke(window.nameId);

            _dict.Remove(window.nameId);
            if (AnimationController != null)
            {
                AnimationController.RemoveWindowAnimation(window);
            }
            RemoveWindowFromQueue(window);
            window.DestroyWindow();
        }

        internal void ReportOpen(UIBaseWindow window)
        {
            OnWindowOpened?.Invoke(window);
        }
        internal void ReportClose(UIBaseWindow window)
        {
            OnWindowClosed?.Invoke(window);
        }

        public bool IsEmptyQuenue()
        {
            return _activeWindowsQueue.Count <= 1;
        }

        public void AddWindowToQueue(UIBaseWindow window)
        {
            _activeWindowsQueue.Remove(window);
            _activeWindowsQueue.Add(window);
        }

        internal void OpenInQueue(UIBaseWindow window)
        {
            if (_activeWindowsQueue.Count > 1)
            {
                if (!_toOpenWindowsQueue.Contains(window))
                {
                    _toOpenWindowsQueue.Enqueue(window);
                }
            }
            else
                window.OpenWindow();
        }

        public void TryOpenWindowFromOpenQueue()
        {
            if (_activeWindowsQueue.Count == 1)
            {
                if (_toOpenWindowsQueue.Count > 0)
                {
                    _toOpenWindowsQueue.Dequeue().OpenWindow();
                }
            }
        }

        public void RemoveWindowFromQueue(UIBaseWindow window)
        {
            _activeWindowsQueue.Remove(window);
            if (_activeWindowsQueue.Count == 1)
                TryOpenWindowFromOpenQueue();

        }

        public void AutoCloseAllWindow()
        {
            for (int i = _activeWindowsQueue.Count - 1; i >= 0; i--)
            {
                var lastWindow = _activeWindowsQueue[i];
                if (!(lastWindow is IUIDisableAutoClose))
                    lastWindow.CloseWindow();
            }
        }

        public void AutoCloseAllWindow<T>()
        {
            for (int i = _activeWindowsQueue.Count - 1; i >= 0; i--)
            {
                var lastWindow = _activeWindowsQueue[i];
                if (!(lastWindow is IUIDisableAutoClose) && !(lastWindow is T))
                    lastWindow.CloseWindow();
            }
        }

        public UIBaseWindow GetLastWindow()
        {
            if (_activeWindowsQueue.Count <= 1)
                return null;

            return _activeWindowsQueue[_activeWindowsQueue.Count - 1];
        }

        public void ForEach<T>(Action<T> action) where T : class
        {
            foreach (var window in _dict.Values)
            {
                if (window is T)
                {
                    action?.Invoke(window as T);
                }
            }
        }

        public int ActiveWindowsCount
        {
            get
            {
                return _activeWindowsQueue.Count;
            }
        }

#endregion
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                int count = _activeWindowsQueue.Count;
                if (count > 0)
                {
                    var lastWindow = _activeWindowsQueue[count - 1];
                    lastWindow.TryCloseWindowByEscapeButton();
                }
            }

//#if UNITY_STANDALONE
//            if (Input.GetKeyDown(KeyCode.Space))
//                this.Canvas.enabled = !this.Canvas.enabled;
//#endif
        }

#region Utils

        protected virtual void Awake()
        {
            Instance = this;
            SetScreenScale();
            DontDestroyOnLoad(this.gameObject);
            foreach (var windowReference in preloadedWindows)
            {
                var window = (Instantiate(windowReference.GetSource(), preloadedWindowsRoot, false));
                AddWindow(window);
            }
        }
        //move to otherclass
        //на глаз, как и форумула matchWidthOrHeight
        const float DEFAULT_SCREEN_RATIO = 16f / 9f;
        const float FACTOR = 4f / 3f;

        [ContextMenu("SetScreenScale")]
        private void SetScreenScale()
        {
            int width = Screen.width;
            int height = Screen.height;
            float scale = (float)Mathf.Max(width, height) / Mathf.Min(width, height);
            if (scale > DEFAULT_SCREEN_RATIO)
            {
                canvasScaler.matchWidthOrHeight = (scale - DEFAULT_SCREEN_RATIO) * FACTOR;
            }
        }

        public int GetLocalIndex(int index)
        {
            int count = indexes.Count();
            for (int i = 0; i < count; i++)
            {
                if (indexes[i] > index)
                {
                    indexes.Insert(i, index);
                    return i;
                }
            }
            indexes.Add(index);
            return count;
        }

#endregion


#region internal
        internal static UIMainController Instance { get; private set; }
        internal UIWindowAnimationController AnimationController => _animationController;
        #endregion
    }
}
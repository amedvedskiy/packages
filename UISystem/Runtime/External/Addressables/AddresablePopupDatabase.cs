﻿#if UISYSTEM_ADDRESSABLES
using Cysharp.Threading.Tasks;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace UISystem
{
    [CreateAssetMenu(fileName = NAME, menuName = Constants.CREATE_ASSET_MENU + NAME)]
    public class AddresablePopupDatabase : BasePopupDatabase
	{
        [System.Serializable]
        public class AddressableReferenceWindow : ComponentReference<UIBaseWindow>
        {
            public AddressableReferenceWindow(string guid) : base(guid) { }
        }

        [System.Serializable]
        public class AssetReferenceWindow
        {
            public string id;
            public AddressableReferenceWindow window;
        }

        public const string NAME = "AddresablePopupDatabase";
		public List<AssetReferenceWindow> popups;


        public override T GetWindow<T>(string name, out int index)
        {
            throw new System.Exception("U try use GetWindow with addressables, please use GetWindowAsync");
        }

        public async UniTask<T> GetWindowAsync<T>(string name) where T: UIBaseWindow
        {
            var asset = FindWindow(name);
            return await asset.window.InstantiateAsync() as T;
        }

        public int GetIndex(string name) => popups.FindIndex(x => x.id == name);

        private AssetReferenceWindow FindWindow(string name) => popups.Find(x => x.id == name);
#if UNITY_EDITOR
        [ContextMenu("FillID")]
        private void FillId()
        {
            foreach (var p in popups)
                p.id = p.window.editorAsset.name;
        }
#endif
    }
}
#endif

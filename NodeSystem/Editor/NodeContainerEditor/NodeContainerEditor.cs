using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
namespace NodeSystem
{
    [CustomEditor(typeof(NodeContainer))]
    public class NodeContainerEditor : BaseNodeContainerEditor<Node>
    {

    }
}

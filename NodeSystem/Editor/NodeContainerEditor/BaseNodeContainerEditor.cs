using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
namespace NodeSystem
{
    public class BaseNodeContainerEditor<T> : Editor where T:Node
    {
        static readonly string PROPERTY_NAME = "nodes";

        protected ReordableNodeList<T> list;

        public INodeContainer<T> Target => (INodeContainer<T>)target;

        protected virtual string GetPropertyName()
        {
            return PROPERTY_NAME;
        }

        protected virtual void OnEnable()
        {
            list = new ReordableNodeList<T>(Target.GetNodes(), serializedObject, serializedObject.FindProperty(GetPropertyName()));
        }

        protected virtual void OnDisable()
        {
            list.OnDisable();
        }

        public override void OnInspectorGUI()
        {
            SerializedProperty prop = serializedObject.GetIterator();
            prop.NextVisible(true);
            GUI.enabled = false;
            //Draw Script
            EditorGUILayout.PropertyField(prop);
            GUI.enabled = true;
            while (prop.NextVisible(false))
            {
                //Skip List
                if (prop.name == GetPropertyName())
                {
                    continue;
                }
                else
                {
                    EditorGUILayout.PropertyField(prop);
                    prop.serializedObject.ApplyModifiedProperties();
                }
            }
            //Draw List
            list.OnInspectorGUI();
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace NodeSystem
{
    /// <summary>
    /// Editor for Node list
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ReordableNodeList<T> where T : Node
    {
        public static Node BufferNode { get; set; }
        public static List<Node> BufferNodes = new List<Node>();
        public ReorderableList NodeList { get; protected set; }

        protected List<Type> nodeTypes = new List<Type>();

        public List<T> TargetList { get; set; }
        public SerializedObject TargetObject { get; set; }
        public SerializedProperty TargetProperty { get; set; }
        public bool hideAll = false;
        public string listName = "";
        const float UNVISIBLE_PROPERTY_SIZE = 20f;

#if UNITY_2019_1_OR_NEWER
        const bool SHOW_VISIBLE_TOGGLE = true;
#else
        const bool SHOW_VISIBLE_TOGGLE = false;
#endif
        private bool _multyInstanceNodesEnabled = true;
        public ReordableNodeList(List<T> target, SerializedObject targetObject, SerializedProperty targetProperty, bool multyInstanceNodesEnabled = true)
        {
            TargetList = target;
            TargetObject = targetObject;
            TargetProperty = targetProperty;
            _multyInstanceNodesEnabled = multyInstanceNodesEnabled;

            //Найти все классы наследники от Node
            var assemblys = AppDomain.CurrentDomain.GetAssemblies();
            foreach (var assembly in assemblys)
            {
                try
                {
                    nodeTypes.AddRange(assembly.GetTypes().Where(x => x.IsSubclassOf(typeof(T)) && !x.IsAbstract));
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }

            //создает ReorderableList
            NodeList = new ReorderableList(TargetObject,
                                            targetProperty,
                                            true, true, true, true);


            NodeList.drawElementCallback = OnDrawElementCallback;
            NodeList.onSelectCallback = OnSelectCallback;
            NodeList.elementHeightCallback = OnElementHeightCallback;
            NodeList.drawHeaderCallback = OnDrawHeaderCallback;
            NodeList.onReorderCallback = OnReorderCallback;
            NodeList.onCanRemoveCallback = OnCanRemoveCallback;
            NodeList.onRemoveCallback = OnRemoveCallback;
            NodeList.onAddDropdownCallback = OnAddDropdownCallback;
        }

        public virtual void OnHideAll(Rect rect)
        {

            hideAll = !EditorGUI.Foldout(rect, !hideAll, GUIContent.none);

            if (hideAll)
            {
                for (int i = 0; i < TargetList.Count; i++)
                {
                    TargetList[i].visible = false;
                    SerializedProperty element = NodeList.serializedProperty.GetArrayElementAtIndex(i);
                    NodeList.elementHeight = CalculatepropertyHeight(element, TargetList[i].visible);
                    hideAll = false;
                }
            }
        }

        public virtual void OnInspectorGUI()
        {
            TargetObject.Update();

            //var rect = GUILayoutUtility.GetRect(0, GetTotalHeight() + UNVISIBLE_PROPERTY_SIZE, GUILayout.ExpandWidth(true));
            //NodeList.DoList(rect);
            NodeList.DoLayoutList();

            TargetObject.ApplyModifiedProperties();

            Event e = Event.current;

            //Copy

            if (e.shift && e.keyCode == KeyCode.C && e.type == EventType.KeyUp)
            {
                //if selected
                if (NodeList.index != -1)
                {
                    CopyNodeToBuffer(TargetList, NodeList.index);
                }
            }

            //Paste
            if (e.shift && e.keyCode == KeyCode.V && e.type == EventType.KeyUp)
            {
                //if selected
                if (NodeList.index != -1 && BufferNode)
                {
                    CopyNode(TargetList, BufferNode, NodeList.index);
                }
            }

            //Duplicate
            if (e.control && e.keyCode == KeyCode.D && e.type == EventType.KeyUp)
            {
                //if selected
                if (NodeList.index != -1)
                {
                    CopyNode(TargetList, NodeList.index);
                }
            }

            //Delete
            if (e.keyCode == KeyCode.Delete && e.type == EventType.KeyUp)
            {
                //if selected
                if (NodeList.index != -1)
                {
                    RemoveWithDialogMenu(NodeList);
                }
            }

            if (e.type == EventType.ContextClick)
            {
                ContextMenu(e);
            }
        }
        const float margin = 2f;
        protected virtual void OnDrawElementCallback(Rect rect, int index, bool isActive, bool isFocused)
        {
            Node node = TargetList[index];

            string[] parts = node.name.Split(' ');
            string name = parts[parts.Length - 1];// display only last part of name
            rect.height = EditorGUIUtility.singleLineHeight;

            if (SHOW_VISIBLE_TOGGLE)
            {
                Rect toogleRect = rect;
                toogleRect.x += 10;
                toogleRect.width = 15f;
                node.visible = EditorGUI.Foldout(toogleRect, node.visible, "");

                Rect labelRect = rect;
                labelRect.width -= 15f;
                labelRect.x += 15f;
                EditorGUI.LabelField(labelRect, name, EditorStyles.boldLabel);
            }
            else
            {
#pragma warning disable CS0162 
                EditorGUI.LabelField(rect, name, EditorStyles.boldLabel);
#pragma warning restore CS0162 
            }

            rect.y += UNVISIBLE_PROPERTY_SIZE + margin;
            rect.height += UNVISIBLE_PROPERTY_SIZE + margin;

            SerializedObject serializedObject = new SerializedObject(node);
            serializedObject.Update();

            if (node.visible)
            {
                EditorGUI.indentLevel++;
                SerializedProperty prop = serializedObject.GetIterator();

                if (prop.NextVisible(true))
                {
                    do
                    {
                        //Don't bother drawing the class file
                        if (prop.name == "m_Script") 
                            continue;

                        float height = EditorGUI.GetPropertyHeight(prop, new GUIContent(prop.displayName), true);
                        EditorGUI.PropertyField(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), prop, true);
                        rect.y += height;
                        rect.height += height;
                        prop.serializedObject.ApplyModifiedProperties();
                    }
                    while (prop.NextVisible(false));
                }
                EditorGUI.indentLevel--;
            }

            serializedObject.ApplyModifiedProperties();

        }
        const int DOUBLE_CLICK_COUNT = 2;
        protected virtual void OnSelectCallback(ReorderableList l)
        {
            if (!SHOW_VISIBLE_TOGGLE)
            {
                //Double click;
#pragma warning disable CS0162 
                if (Event.current.clickCount == DOUBLE_CLICK_COUNT)
                {
                    TargetList[l.index].visible = !TargetList[l.index].visible;
                }
#pragma warning restore CS0162 
            }
        }


        protected virtual float OnElementHeightCallback(int index)
        {
            if (TargetList.Count == 0 || index >= TargetList.Count)
            {
                return CalculatepropertyHeight(null, false);
            }
            if (TargetList[index].visible)
            {
                SerializedProperty element = GetPropertyAtIndex(index);
                return CalculatepropertyHeight(element, TargetList[index].visible);
            }
            else
            {
                return CalculatepropertyHeight(null, TargetList[index].visible);
            }
        }

        protected float GetTotalHeight()
        {
            float height = 0;
            for (int i = 0; i < TargetList.Count; i++)
            {
                height += OnElementHeightCallback(i);
            }
            return height;
        }

        protected virtual void OnDrawHeaderCallback(Rect rect)
        {
            EditorGUI.LabelField(new Rect(rect.x, rect.y, rect.width * 0.5f, rect.height), TargetProperty.name);
            //_multyInstanceNodesEnabled = EditorGUI.Toggle(new Rect(rect.x + rect.width * 0.5f, rect.y, rect.width * 0.5f, rect.height), "MultyNodesEnabled", _multyInstanceNodesEnabled);
            OnHideAll(new Rect(rect.x + (rect.width - 10f), rect.y, 10f, rect.height));
        }

        protected virtual void OnAddDropdownCallback(Rect buttonRect, ReorderableList l)
        {
            GenericMenu nodesMenu = new GenericMenu();
            //toolsMenu.AddSeparator("");
            foreach (var type in nodeTypes)
            {
                if(!_multyInstanceNodesEnabled && TargetList.Exists(x => x.GetType() == type))
                {
                    continue;
                }

                NodeGroupAttribute group = (NodeGroupAttribute)Attribute.GetCustomAttribute(type, typeof(NodeGroupAttribute));
                string groupName = group != null ? string.Format("{0}/", group.groupName) : string.Format("{0}/", NodeGroupAttribute.GroupConstants.OTHER);
                nodesMenu.AddItem(new GUIContent(groupName + type.Name), false, OnSelectCreateItem, type);
            }
            nodesMenu.ShowAsContext();
        }

        protected virtual void OnReorderCallback(ReorderableList l)
        {
            RenameNodes(TargetList);
        }

        protected virtual bool OnCanRemoveCallback(ReorderableList l)
        {
            return l.count > 0;
        }

        protected virtual void OnRemoveCallback(ReorderableList l)
        {
            RemoveWithDialogMenu(l);
        }

        protected float CalculatepropertyHeight(SerializedProperty property, bool visible = true)
        {
            const float SPACE = 5f;

            if (!visible)
            {
                return UNVISIBLE_PROPERTY_SIZE + SPACE;
            }
            SerializedObject objectSerialized = new SerializedObject(property.objectReferenceValue);
            SerializedProperty prop = objectSerialized.GetIterator();

            float margin = 2f;
            float height = 0;
            prop.NextVisible(true);
            height += EditorGUI.GetPropertyHeight(prop) + margin;
            while (prop.NextVisible(false))
            {
                height += EditorGUI.GetPropertyHeight(prop) + margin;
            }
            return height + SPACE;
        }

        protected void OnSelectCreateItem(object type)
        {
            if (type is Type == false)
                return;

            AddNode(TargetList, type as Type);
        }

        protected void AddNode(List<T> listContainer, Type nodeType)
        {
            var node = ScriptableObject.CreateInstance(nodeType) as T;
            //node.name = string.Format("{0}/{1} {2}", TargetObject.targetObject.name, listContainer.Count, nodeType.Name);
            node.name = string.Format("{0}{1}/{2} {3}", TargetObject.targetObject.name, listName, listContainer.Count, nodeType.Name);
            //node.hideFlags = HideFlags.HideInInspector | HideFlags.HideInHierarchy;
            listContainer.Add(node);
            AssetDatabase.AddObjectToAsset(node, TargetObject.targetObject);
            Save();
        }

        protected void RemoveNode(List<T> nodeContainer, int index)
        {
            if (nodeContainer.Count <= 0)
                return;

            var node = nodeContainer[index];
            nodeContainer.Remove(node);
            BufferNodes.RemoveAll(n => n.Equals(node));
            UnityEngine.Object.DestroyImmediate(node, true);

            Save();
        }

        public void RenameNodes(List<T> nodeContainer)
        {
            for (int i = 0; i < nodeContainer.Count; i++)
            {
                var node = nodeContainer[i];
                //node.name = string.Format("{0}/{1} {2}", TargetObject.targetObject.name, i, node.GetType().Name);
                node.name = string.Format("{0}{1}/{2} {3}", TargetObject.targetObject.name, listName, i+1, node.GetType().Name);
            }
            Save();
        }

        protected void CopyNode(List<T> listContainer, int index)
        {
            T node = listContainer[index];
            CopyNode(listContainer, node, index);
        }

        protected void CopyNodeToBuffer(List<T> listContainer, int index)
        {
            T node = listContainer[index];
            BufferNode = node;
        }

        protected void PasteNodeByInstanceID(int id)
        {
            Node node = (Node)EditorUtility.InstanceIDToObject(id);
            if (node)
                CopyNode(TargetList, node, NodeList.index);
            else
                Debug.LogWarningFormat("Wrong InstanceID \"{0}\".", id);
        }

        protected void CopyNode(List<T> listContainer, Node node, int index)
        {
            Type type = node.GetType();

            T copyNode = InsertNode(listContainer, type, index++);

            SerializedObject serializedNode = new SerializedObject(node);
            SerializedObject serializedCopyNode = new SerializedObject(copyNode);

            SerializedProperty prop = serializedNode.GetIterator();
            prop.NextVisible(true);

            while (prop.NextVisible(false))
            {
                serializedCopyNode.CopyFromSerializedProperty(prop);
            }
            serializedCopyNode.ApplyModifiedProperties();
            RenameNodes(listContainer);
        }

        protected T InsertNode(List<T> listContainer, Type nodeType, int index)
        {
            var node = ScriptableObject.CreateInstance(nodeType) as T;
            //node.name = string.Format("{0}/{1} {2}", TargetObject.targetObject.name, listContainer.Count, nodeType.Name);
            node.name = string.Format("{0}{1}/{2} {3}", TargetObject.targetObject.name, listName, listContainer.Count, nodeType.Name);
            //node.hideFlags = HideFlags.HideInInspector | HideFlags.HideInHierarchy;
            listContainer.Insert(index, node);

            AssetDatabase.AddObjectToAsset(node, TargetObject.targetObject);
            Save();
            return node;
        }

        protected void RemoveWithDialogMenu(ReorderableList l)
        {
            if (l.index == -1)
                return;

            EditorApplication.Beep();
            if (EditorUtility.DisplayDialog("Remove Node", string.Format("Do you want remove {0}?", TargetList[l.index].name), "Yes", "Cancel"))
            {
                RemoveNode(TargetList, l.index);
            }
        }

        protected void ContextMenu(Event e)
        {
            if (NodeList.index != -1)
            {
                GenericMenu menu = new GenericMenu();

                menu.AddItem(new GUIContent("Copy Selected"), false, () => CopyNodeToBuffer(TargetList, NodeList.index));
                if (BufferNode)
                {
                    menu.AddItem(new GUIContent("Past Above Selected"), false, () => CopyNode(TargetList, BufferNode, NodeList.index));
                    menu.AddItem(new GUIContent("Past Under Selected"), false, () => CopyNode(TargetList, BufferNode, NodeList.index + 1));
                }
                menu.AddSeparator("");
                menu.AddItem(new GUIContent("Add Selected Node To Buffer"), false, () => BufferNodes.Add(TargetList[NodeList.index]));

                if (BufferNodes.Count > 0)
                {
                    menu.AddItem(new GUIContent(string.Format("{0}\t({1})", "Past Nodes from Buffer", BufferNodes.Count)), false, () =>
                        {
                            int i = 0;
                            foreach (var node in BufferNodes)
                            {
                                i++;
                                CopyNode(TargetList, node, NodeList.index + i);
                            }
                        }
                    );
                    menu.AddItem(new GUIContent("Clear Buffer"), false, () => BufferNodes.Clear());
                }
                menu.ShowAsContext();
                e.Use();
            }
        }

        protected Dictionary<int, SerializedProperty> props = new Dictionary<int, SerializedProperty>();

        public SerializedProperty GetPropertyAtIndex(int index)
        {
            if (props.ContainsKey(index))
            {
                var p = props[index];
                p.serializedObject.Update();
                return p;
            }

            var prop = TargetProperty.GetArrayElementAtIndex(index);
            props.Add(index, prop);
            return prop;
        }

        bool needSave = false;

        void Save()
        {
            needSave = true;
        }

        public void OnDisable()
        {
            if (needSave)
            {
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
                needSave = false;
            }
        }
    }
}


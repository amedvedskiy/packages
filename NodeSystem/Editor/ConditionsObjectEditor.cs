﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace NodeSystem
{
    [CustomEditor(typeof(ConditionsObject))]
    public class ConditionsObjectEditor : BaseNodeContainerEditor<ConditionNode>
    {
        static readonly string PROPERTY_NAME = "conditions";

        protected override string GetPropertyName()
        {
            return PROPERTY_NAME;
        }
    }
}

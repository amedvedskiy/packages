﻿using UnityEngine;

namespace NodeSystem
{
    public enum CompareType
    {
        Greater,
        Equal,
        Less,
        NotEqual
    }

    public static class CompareTypeUtility
    {
        public static bool Compare(this CompareType type, int value, int to)
        {
            switch (type)
            {
                case CompareType.Greater:
                    return value > to;
                case CompareType.Equal:
                    return value == to;
                case CompareType.NotEqual:
                    return value != to;
                case CompareType.Less:
                    return value < to;
                default:
                    Debug.LogError("Wrong CompareType");
                    return false;
            }
        }
        public static bool Compare(this CompareType type, float value, float to)
        {
            switch (type)
            {
                case CompareType.Greater:
                    return value > to;
                case CompareType.Equal:
                    return value == to;
                case CompareType.NotEqual:
                    return value != to;
                case CompareType.Less:
                    return value < to;
                default:
                    Debug.LogError("Wrong CompareType");
                    return false;
            }
        }
        public static bool Compare(this CompareType type, string value, string to)
        {
            switch (type)
            {
                case CompareType.Equal:
                    return value == to;
                case CompareType.NotEqual:
                    return value != to;
                default:
                    Debug.LogError("Wrong CompareType");
                    return false;
            }
        }
    }

}
﻿using UnityEngine;
using System;
namespace NodeSystem
{
    [Serializable]
    public class Node : ScriptableObject
    {
        public new string name
        {
            get
            {
                return base.name;
            }
            set
            {
                base.name = value;
            }
        }

        public new HideFlags hideFlags
        {
            get
            {
                return base.hideFlags;
            }
            set
            {
                base.hideFlags = value;
            }
        }

        public virtual Node Clone()
        {
            return Instantiate(this);
        }

        //Use for editor
        [HideInInspector]
        public bool enabled = true;
        [HideInInspector]
        public bool visible = true;
    }
}

﻿using System;
namespace NodeSystem
{
    [Serializable]
    public class ConditionNode : Node
    {
        protected bool _state = false;

        public virtual void Update()
        {

        }

        public virtual bool GetState()
        {
            return _state;
        }
    }
}

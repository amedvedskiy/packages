﻿namespace NodeSystem
{
    /// <summary>
    /// Атрибут для разделение Node по группам
    /// </summary>
    public class NodeGroupAttribute : System.Attribute
    {
        public partial class GroupConstants
        {
            public const string TEST = "Other/Test";
            public const string OTHER = "Other";
            public const string DEBUG = "Debug";
        }

        public string groupName;

        public NodeGroupAttribute(string name)
        {
            groupName = name;
        }
    }
}


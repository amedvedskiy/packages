﻿using System.Collections.Generic;
using UnityEngine;
namespace NodeSystem
{
    [CreateAssetMenu(menuName = "Scriptable Objects/ConditionsObject")]
    public class ConditionsObject : ConditionNode, INodeContainer<ConditionNode>
    {
        public Comparison comparison;
        [Expanded]
        public List<ConditionNode> conditions = new List<ConditionNode>();

        public List<ConditionNode> GetNodes()
        {
            return conditions;
        }

        public override bool GetState()
        {
            switch (comparison)
            {
                case Comparison.TrueForAll:
                    return conditions.TrueForAll(StatePredicate);
                case Comparison.TrueForAny:
                    return conditions.Exists(StatePredicate);
                case Comparison.FalseForAny:
                    return !conditions.Exists(StatePredicate);
                case Comparison.FalseForAll:
                    return !conditions.TrueForAll(StatePredicate);
                default:
                    return false;
            }
        }

        [ContextMenu("Log Availability")]
        public void LogIsAvailable()
        {
            Debug.Log(GetState());
        }

        bool StatePredicate(ConditionNode condition)
        {
            condition.Update();
            return condition.GetState();
        }

        public enum Comparison
        {
            TrueForAll,
            TrueForAny,
            FalseForAll,
            FalseForAny
        }
    }
}
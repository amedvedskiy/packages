﻿using System;

namespace NodeSystem
{
    [Serializable]
    public class DummyCondition : ConditionNode
    {
        public bool state;

        public override void Update()
        {
            _state = state;
        }
        public override bool GetState()
        {
            return base.GetState();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace NodeSystem
{
    [NodeGroup(NodeGroupAttribute.GroupConstants.TEST)]
    public class PlayerPrefsCondition : ConditionNode
    {
        public bool equal = true;

        public string key;
        public FieldType fieldType;
        public int intValue;
        public float floatValue;
        public string stringValue;

        public override bool GetState()
        {
            switch (fieldType)
            {
                case FieldType.Int:
                    return PlayerPrefs.GetInt(key) == intValue && equal;
                case FieldType.Float:
                    return PlayerPrefs.GetFloat(key) == floatValue && equal;
                case FieldType.String:
                    return PlayerPrefs.GetString(key).Equals(stringValue) && equal;
                default:
                    return !equal;
            }
        }

        public enum FieldType
        {
            Int,
            Float,
            String
        }
    }
}

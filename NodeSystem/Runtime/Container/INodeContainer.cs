using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace NodeSystem
{
    public interface INodeContainer<T>
    {
        List<T> GetNodes();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NodeSystem
{
    public abstract class BaseNodeContainer<T>:Node, INodeContainer<T> where T:Node
    {
        public List<T> nodes;

        public List<T> GetNodes()
        {
            return nodes;
        }
    }
}

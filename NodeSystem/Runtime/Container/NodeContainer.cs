using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NodeSystem
{
    [CreateAssetMenu(menuName = "Scriptable Objects/NodeContainer")]
    public class NodeContainer : BaseNodeContainer<Node>
    {

    }
}

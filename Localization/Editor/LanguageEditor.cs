using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using UnityEngine.Networking;

namespace Localization
{
    public class LanguageEditor : EditorWindow
    {
        //Settings
        public LocalizationSettings settings = null;

        [MenuItem("Tools/Localization/LoadLocalization")]
        static void OpenWindow()
        {
            EditorWindow.GetWindow(typeof(LanguageEditor));
        }

        string gDocsURL = string.Empty;
        int unresolvedErrors = 0;
        int foundSheets = 0;
        bool useSystemLang = true;
        LanguageCode langCode = LanguageCode.EN;
        bool loadedSettings = false;
        string predefSheetTitle;

        bool updatingTranslation;
        int parsingXMLCount;
        string status = "...";
        string selectedSheet = "";

        Vector2 scrollView;

        void OnGUI()
        {
            if (EditorApplication.isPlaying)
            {
                GUILayout.Label("Editor is in play mode.");
                return;
            }
            loadedSettings = false;
            LoadSettings();
            if (settings == null)
            {
                GUILayout.Label("Settings not found " + LocalizationSettings.SETTINGS_ASSET_PATH);
                return;
            }


            scrollView = GUILayout.BeginScrollView(scrollView);

            GUILayout.Label("Settings", EditorStyles.boldLabel);
            useSystemLang = EditorGUILayout.Toggle("Try system language", useSystemLang);
            langCode = (LanguageCode)EditorGUILayout.EnumPopup("Default language", langCode);

            int index = settings.sheetInfos.FindIndex(x => x.name == predefSheetTitle);
            if (index > -1)
            {
                index = EditorGUILayout.Popup("PreDefault Sheet Title", index, settings.sheetInfos.Select(x => x.name).ToArray());
                predefSheetTitle = settings.sheetInfos[index].name;
            }
            else
            {
                predefSheetTitle = EditorGUILayout.DelayedTextField("PreDefault Sheet Title", predefSheetTitle);
            }

            gDocsURL = EditorGUILayout.TextField("gDocs Link", gDocsURL);

            if (GUI.changed)
            {
                SaveSettingsFile();
            }

            GUILayout.Space(10f);
            if (status != null)
            {
                GUILayout.Label(status, EditorStyles.label);
            }

            if (!updatingTranslation)
            {
                if (GUILayout.Button("Update All translations"))
                {
                    updatingTranslation = true;
                    UpdateLocalization();
                }

                GUILayout.Space(10f);

                int selectSheetIndex = settings.sheetInfos.FindIndex(x => x.name == selectedSheet);
                selectSheetIndex = EditorGUILayout.Popup("Selected Sheet Title", selectSheetIndex, settings.sheetInfos.Select(x => x.name).ToArray());

                if (selectSheetIndex != -1)
                    selectedSheet = settings.sheetInfos[selectSheetIndex].name;

                //update single sheet;
                if (selectSheetIndex != -1 && GUILayout.Button("Update Selected translation"))
                {
                    updatingTranslation = true;
                    var info = settings.sheetInfos.FirstOrDefault(x => x.name == selectedSheet);
                    UpdateSheet(info);
                    updatingTranslation = false;
                }
            }

            if (unresolvedErrors > 0)
            {
                Rect rec = GUILayoutUtility.GetLastRect();
                GUI.color = Color.red;
                EditorGUI.DropShadowLabel(new Rect(0, rec.yMin + 15, 200, 20), "Unresolved errors: " + unresolvedErrors);
                GUI.color = Color.white;
                updatingTranslation = false;
            }

            GUILayout.Space(10f);
            //DrawUtility();
            GUILayout.EndScrollView();
        }

        void UpdateLocalization()
        {
            foreach (var info in settings.sheetInfos)
            {
                UpdateSheet(info);
            }
            updatingTranslation = false;
        }

        void UpdateSheet(LocalizationSettings.SheetInfo info)
        {
            string url = string.Format("{0}&gid={1}", settings.documentUrl, info.id);
            var request = UnityWebRequest.Get(url);
            var async = request.SendWebRequest();

            Debug.Log("Start Loading " + info.name);

            while (!async.isDone)
            {
                EditorUtility.DisplayProgressBar("Loading", info.name, async.progress);
                System.Threading.Thread.Sleep(1000);
            }
            EditorUtility.ClearProgressBar();

            if (request.result == UnityWebRequest.Result.ConnectionError)
            {
                Debug.Log("isNetworkError " + info.name);
            }
            else
            {
                var data = request.downloadHandler.text;
                Debug.Log("Start Parsing " + info.name);
                ParseData(data, info.name);
                Debug.Log("Complete Parsing " + info.name);
            }
        }

        void LoadSettings()
        {
            if (loadedSettings || settings != null)
                return;


            if (File.Exists(LocalizationSettings.SETTINGS_ASSET_PATH))
            {
                settings = AssetDatabase.LoadAssetAtPath<LocalizationSettings>(LocalizationSettings.SETTINGS_ASSET_PATH);
            }
            else
            {
                Debug.LogError("does not exist, Create new");
                LocalizationSettings newSettings = ScriptableObject.CreateInstance<LocalizationSettings>();
                if (!AssetDatabase.IsValidFolder(LocalizationSettings.ASSET_RESOURCES_PATH))
                {
                    AssetDatabase.CreateFolder("Assets", "Localization");
                    AssetDatabase.CreateFolder("Assets/Localization", "Resources");
                    AssetDatabase.CreateFolder("Assets/Localization/Resources", "Languages");
                }
                AssetDatabase.CreateAsset(newSettings, LocalizationSettings.SETTINGS_ASSET_PATH);
                AssetDatabase.Refresh();

                settings = AssetDatabase.LoadAssetAtPath<LocalizationSettings>(LocalizationSettings.SETTINGS_ASSET_PATH);
            }

            loadedSettings = true;
            useSystemLang = settings.useSystemLanguagePerDefault;
            langCode = settings.defaultLangCode;
            predefSheetTitle = settings.predefSheetTitle;
            gDocsURL = settings.documentUrl;
        }

        void LoadCSV(Hashtable loadLanguages, Hashtable loadEntries, string data, string sheetTitle)
        {
            List<string> lines = GetCVSLines(data);

            for (int i = 0; i < lines.Count; i++)
            {
                string line = lines[i];
                List<string> contents = GetCVSLine(line);
                if (i == 0)
                {//Language titles
                    for (int j = 1; j < contents.Count; j++)
                    {
                        loadLanguages[j] = contents[j];
                        loadEntries[j] = new Hashtable();
                    }
                }
                else if (contents.Count > 1)
                {
                    string key = contents[0];
                    if (key == "")
                        continue; //Skip entries with empty keys (the other values can be used as labels)
                    for (int j = 1; j < (loadLanguages.Count + 1) && j < contents.Count; j++)
                    {

                        string content = contents[j];
                        Hashtable hTable = (Hashtable)loadEntries[j];
                        if (hTable.ContainsKey(key))
                        {
                            Debug.LogError("ERROR: Double key [" + key + "] Sheet: " + sheetTitle);
                            unresolvedErrors++;
                        }
                        hTable[key] = System.Security.SecurityElement.Escape(content);
                    }
                }
            }
        }

        bool ParseData(string data, string sheetTitle)
        {
            string langFolder = GetLanguageFolder();

            Hashtable loadLanguages = new Hashtable();
            Hashtable loadEntries = new Hashtable();

            LoadCSV(loadLanguages, loadEntries, data, sheetTitle);

            if (loadEntries.Count < 1)
            {
                unresolvedErrors++;
                Debug.LogError("Sheet " + sheetTitle + " contains no languages!");
                return false;
            }

            //Verify loaded data
            Hashtable sampleData = (Hashtable)loadEntries[1];
            for (int j = 2; j < loadEntries.Count; j++)
            {
                Hashtable otherData = ((Hashtable)loadEntries[j]);

                foreach (DictionaryEntry item in otherData)
                {
                    if (!sampleData.ContainsKey(item.Key))
                    {
                        Debug.LogError("[" + loadLanguages[1] + "] [" + item.Key + "] Key is missing!");
                        unresolvedErrors++;
                    }
                }
                foreach (DictionaryEntry item in sampleData)
                {
                    if (!otherData.ContainsKey(item.Key))
                    {
                        Debug.LogError("Sheet(" + sheetTitle + ") [" + loadLanguages[j] + "] [" + item.Key + "] Key is missing!");
                        unresolvedErrors++;
                    }
                }
            }

            //Save the loaded data
            foreach (DictionaryEntry langs in loadLanguages)
            {
                LanguageAsset asset = CreateInstance<LanguageAsset>();

                string langCode = ((string)langs.Value).TrimEnd(System.Environment.NewLine.ToCharArray());
                if (string.IsNullOrEmpty(langCode))
                    continue;

                LanguageCode lc = (LanguageCode)System.Enum.Parse(typeof(LanguageCode), langCode);
                if (!settings.languageFilter.Exists(x => x == lc))
                    continue;

                int langID = (int)langs.Key;
                Hashtable entries = (Hashtable)loadEntries[langID];
                foreach (DictionaryEntry item in entries)
                {
                    asset.values.Add(new LanguageAsset.LanguageData() { key = item.Key + "", value = (item.Value + "").UnescapeXML() });
                }

                foundSheets++;

                if (sheetTitle != settings.predefSheetTitle)
                    AssetDatabase.CreateAsset(asset, string.Format("{0}{1}_{2}.asset", settings.otherSheetsPath, langCode, sheetTitle));
                else
                    AssetDatabase.CreateAsset(asset, string.Format("{0}{1}_{2}.asset", settings.predefPath, langCode, sheetTitle));
            }

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            return true;
        }


        List<string> GetCVSLines(string data)
        {
            List<string> lines = new List<string>();
            int i = 0;
            int searchCloseTags = 0;
            int lastSentenceStart = 0;
            while (i < data.Length)
            {
                if (data[i] == '"')
                {
                    if (searchCloseTags == 0)
                        searchCloseTags++;
                    else
                        searchCloseTags--;
                }
                else if (data[i] == '\n')
                {
                    if (searchCloseTags == 0)
                    {
                        lines.Add(data.Substring(lastSentenceStart, i - lastSentenceStart));
                        lastSentenceStart = i + 1;
                    }
                }
                i++;
            }
            if (i - 1 > lastSentenceStart)
            {
                lines.Add(data.Substring(lastSentenceStart, i - lastSentenceStart));
            }
            return lines;
        }

        List<string> GetCVSLine(string line)
        {
            List<string> list = new List<string>();
            int i = 0;
            int searchCloseTags = 0;
            int lastEntryBegin = 0;
            while (i < line.Length)
            {
                if (line[i] == '"')
                {
                    if (searchCloseTags == 0)
                        searchCloseTags++;
                    else
                        searchCloseTags--;
                }
                else if (line[i] == ',')
                {
                    if (searchCloseTags == 0)
                    {
                        list.Add(StripQuotes(line.Substring(lastEntryBegin, i - lastEntryBegin)));
                        lastEntryBegin = i + 1;
                    }
                }
                i++;
            }
            if (line.Length > lastEntryBegin)
            {
                list.Add(StripQuotes(line.Substring(lastEntryBegin)));//Add last entry
            }
            return list;
        }

        //Remove the double " that CVS adds inside the lines, and the two outer " as well
        string StripQuotes(string input)
        {
            if (input.Length < 1 || input[0] != '"')
                return input;//Not a " formatted line

            string output = "";
            ;
            int i = 1;
            bool allowNextQuote = false;
            while (i < input.Length - 1)
            {
                string curChar = input[i] + "";
                if (curChar == "\"")
                {
                    if (allowNextQuote)
                        output += curChar;
                    allowNextQuote = !allowNextQuote;
                }
                else
                {
                    output += curChar;
                }
                i++;
            }
            return output;
        }

        string GetLanguageFolder()
        {
            string[] subdirEntries = Directory.GetDirectories(Application.dataPath, "Languages", SearchOption.AllDirectories);
            foreach (string subDir in subdirEntries)
            {
                if (subDir.Contains("Resources"))
                {
                    string outp = subDir.Replace(Application.dataPath, "");
                    if (outp[0] == '\\') outp = outp.Substring(1);
                    return outp;
                }
            }

            //Create folder
            string folder = Application.dataPath + "/Localization";
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);

            folder = folder + "/Resources";
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
                AssetDatabase.ImportAsset(folder, ImportAssetOptions.ForceUpdate);
            }
            folder = folder + "/Languages";
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
                AssetDatabase.ImportAsset(folder, ImportAssetOptions.ForceUpdate);
            }
            AssetDatabase.Refresh();

            return folder;
        }

        void SaveSettingsFile()
        {
            if (settings == null)
            {
                settings = (LocalizationSettings)ScriptableObject.CreateInstance(typeof(LocalizationSettings));
                string settingsPath = Path.GetDirectoryName(LocalizationSettings.SETTINGS_ASSET_PATH);
                if (!Directory.Exists(settingsPath))
                {
                    Directory.CreateDirectory(settingsPath);
                    AssetDatabase.ImportAsset(settingsPath);
                    AssetDatabase.CreateAsset(settings, LocalizationSettings.SETTINGS_ASSET_PATH);
                }
                else
                {
                    AssetDatabase.SaveAssets();
                }
            }
            settings.defaultLangCode = langCode;
            settings.useSystemLanguagePerDefault = useSystemLang;
            settings.predefSheetTitle = predefSheetTitle;

            EditorUtility.SetDirty(settings);

        }

        #region LocalizationFontAssetsUtility

        //public LocalizationFontAssetsUtility utility;

        //void DrawUtility()
        //{
        //    if (utility == null)
        //    {
        //        utility = AssetDatabase.LoadAssetAtPath<LocalizationFontAssetsUtility>(LocalizationFontAssetsUtility.PATH);
        //        if (utility == null && GUILayout.Button("Create LocalizationFontAssetsUtility"))
        //        {
        //            utility = CreateInstance<LocalizationFontAssetsUtility>();
        //            AssetDatabase.CreateAsset(utility, LocalizationFontAssetsUtility.PATH);
        //            AssetDatabase.SaveAssets();
        //            AssetDatabase.Refresh();
        //            utility = AssetDatabase.LoadAssetAtPath<LocalizationFontAssetsUtility>(LocalizationFontAssetsUtility.PATH);
        //        }
        //    }
        //    else
        //    {
        //        EditorGUILayout.ObjectField("Utility", utility, typeof(LocalizationFontAssetsUtility), false);
        //    }
        //}

        #endregion
    }
}
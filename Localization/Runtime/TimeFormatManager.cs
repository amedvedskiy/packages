﻿using System;
using System.Text;
using UnityEngine;

namespace Localization.Utilities
{
    public static class TimeFormatManager
    {
        public enum TimeFrom
        {
            Seconds,
            Minutes,
            Hours,
            Days
        }

        const string TIME_FORMAT_SECONDS = "TimerFormat.Seconds";
        const string TIME_FORMAT_MINUTES = "TimerFormat.Minutes";
        const string TIME_FORMAT_HOURS = "TimerFormat.Hours";
        const string TIME_FORMAT_DAYS = "TimerFormat.Days";
        const string LOCALIZATION_SHEET_COMMON = "Common";

        public static string FormatTime(TimeFrom formatFrom, int timeInSeconds)
        {
            var timeSpan = new TimeSpan(0, 0, timeInSeconds);
            switch (formatFrom)
            {
                case TimeFrom.Days:
                    return GetDaysFormat(timeSpan);
                case TimeFrom.Hours:
                    return GetHoursFormat(timeSpan);
                case TimeFrom.Minutes:
                    return GetMinutesFormat(timeSpan);
                default:
                    return GetSecondsFormat(timeSpan);
            }
        }

        private static string GetSecondsFormat(TimeSpan time)
        {
            string text = Language.Get(TIME_FORMAT_SECONDS, LOCALIZATION_SHEET_COMMON);
            return string.Format(text, time.TotalSeconds);
        }

        private static string GetMinutesFormat(TimeSpan time)
        {
            var ts = time;
            string text = Language.Get(TIME_FORMAT_MINUTES, LOCALIZATION_SHEET_COMMON);
            int minutes = (int)ts.TotalMinutes;
            if (minutes != 0)
                return string.Format(text, minutes, ts.Seconds);
            else
                return GetSecondsFormat(time);
        }

        private static string GetHoursFormat(TimeSpan time)
        {
            var ts = time;
            string text = Language.Get(TIME_FORMAT_HOURS, LOCALIZATION_SHEET_COMMON);
            int hours = (int)ts.TotalHours;
            if (hours != 0)
                return string.Format(text, hours, ts.Minutes);
            else
                return GetMinutesFormat(time);
        }

        private static string GetDaysFormat(TimeSpan time)
        {
            var ts = time;
            string text = Language.Get(TIME_FORMAT_DAYS, LOCALIZATION_SHEET_COMMON);
            int days = (int)ts.TotalDays;
            if (days != 0)
                return string.Format(text, days, ts.Hours);
            else
                return GetHoursFormat(time);
        }
    }
}
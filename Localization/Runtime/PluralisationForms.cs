﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
namespace Localization
{
	public class PluralisationForms
	{

		public const string LOCALIZATION_SERVICE_KEY_PLURAL_FORMS = "plural_forms";
		public const string LOCALIZATION_SHEET_COMMON = "Common";

		private Regex[] _regexs;

		public PluralisationForms()
		{
			var patterns = Language.Get(LOCALIZATION_SERVICE_KEY_PLURAL_FORMS, LOCALIZATION_SHEET_COMMON).Split(';');
			_regexs = new Regex[patterns.Length];
			for (int x = 0; x < patterns.Length; x++)
			{
				_regexs[x] = new Regex(patterns[x]);
			}
		}

		public string GetPluralizedKey(string key, string number)
		{
			for (int x = 0; x < _regexs.Length; x++)
			{
				if (_regexs[x].IsMatch(number))
				{
					return string.Format("{0}.pf{1}", key, x);
				}
			}
			return string.Format("{0}.pf{1}", key, _regexs.Length);
		}

	}
}
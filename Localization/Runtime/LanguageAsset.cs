﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Localization
{
    public class LanguageAsset : ScriptableObject
    {
        [System.Serializable]
        public struct LanguageData
        {
            public string key;
            public string value;
        }

        public List<LanguageData> values = new List<LanguageData>();
    }
}

﻿namespace Localization
{
    [System.Serializable]
    public class KeySheetPair
    {
        public string key;
        public string sheet;

        public override string ToString()
        {
            return Language.Get(key, sheet);
        }
    }
}

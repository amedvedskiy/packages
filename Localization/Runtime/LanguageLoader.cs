﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Localization
{
    public class LanguageLoader
    {
        /// <summary>
        /// Get Language file asse;
        /// </summary>
        public virtual LanguageAsset GetLanguageFileAsset(LanguageCode currentLanguage, string sheetTitle)
        {
            return Resources.Load<LanguageAsset>(string.Format("Languages/{0}_{1}", currentLanguage, sheetTitle));
        }

        /// <summary>
        /// For default all languages aldready has own textures. User custom loader for asian languages
        /// </summary>
        public virtual bool NeedToLoadAtlasses(LanguageCode code)
        {
            return false;
        }

        public virtual void LoadAtlasses(LanguageCode code)
        {
        }

        public virtual void LoadAdditiveLanguage(LanguageCode code, Action<bool> callback)
        {
            callback(true);
        }
    }
}
